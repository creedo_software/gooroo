const { NODE_ENV, BABEL_ENV } = process.env

module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        loose: true,
        modules: false,
        forceAllTransforms: true,
      },
    ],
    '@babel/preset-react',
  ],
  plugins: ['@babel/plugin-transform-modules-commonjs', "@babel/plugin-proposal-class-properties"],
}
