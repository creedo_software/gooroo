import React, { Component } from 'react';
import { Redirect } from "react-router-dom"; 

import Authmanager from 'react-authmanager';

// how to login the user from the server and get a token back
Authmanager.config.fetchToken = async credentials => {
	const response = await fetch(process.env.API_HOST + '/api/login', {
	    method: 'POST', 
	    mode: 'cors', 
	    cache: 'no-cache', 
	    credentials: 'same-origin', 
	    headers: {
	      'Content-Type': 'application/json'
	    },
	    redirect: 'follow', 
	    referrerPolicy: 'no-referrer', 
	    body: JSON.stringify(credentials) 
	});
	const result = await response.json(); 
	console.log('LOGIN RESULT', result)
	return result['access_token'];
}
 
// how to get the current logged user informations from the server
Authmanager.config.fetchUser = async () => {
	const response = await fetch(process.env.API_HOST + '/api/me', {
		headers: new Headers({
	    	'Authorization': 'Bearer ' + Authmanager.getToken() // returns null if no token is stored
	  	}),
	})
	const user = await response.json(); 
	console.log('FETCH USER', user)
 	return user;
}

Authmanager.addGuard('loggedGuard', function(user, props, next) {
	if (user.loading)
		return (<p>LOADING USER</p>); // render a loading component if user is currently fetched from the server

	if (user.logged)
		return next(); // render the component if user is not loading and is logged

	return (<Redirect to="/login" />); // render a login component by default (if user is fetched from the server but not logged)
});

export const Conf = Authmanager.conf