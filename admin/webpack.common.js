const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack');

require('dotenv').config()
console.log('PROCESS ENV', process.env)
module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
        filename: 'index.js',
        publicPath: '/',
    },
  plugins: [
    new webpack.DefinePlugin({  // plugin to define global constants
        "process.env": {
            'PUBLIC_URL': JSON.stringify(process.env.PUBLIC_URL),
            'API_HOST': JSON.stringify(process.env.API_HOST)
        },
        PUBLIC_URL: JSON.stringify(process.env.PUBLIC_URL),
        API_HOST: JSON.stringify(process.env.API_HOST)
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({ template: path.resolve(__dirname, 'public/index.html') }),
    new Dotenv(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: ['babel-loader'],
      },
      {
         test: /\.(woff(2)?|ttf|eot|png|svg|jpg|gif|json|ico)$/,
         use: [
           'file-loader',
         ],
      },
      {
            test: /\.css$/,
            loader: 'style-loader!css-loader'
        },
        {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
              modules: {
                localIdentName: "[local]___[hash:base64:5]"
              }
            }
          },
          {
            loader: "less-loader"
          }
        ]
      }
    ],
  },
  devServer: {
    port: 5000,
    hot: true,
    historyApiFallback: true,
  },
}
