<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller{
    // add these at the top of the file

    public function __construct()
    {
      $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
      return UserResource::collection(User::paginate($request->input('limit', 10)));
    }
    
    public function me(Request $request){
        return response()->json([
            'email' => 'admin@example.com',
            'name' => 'Admin'
        ]);
    }

    public function store(Request $request)
    {
      $user = User::create([
        'name' => $request->name,
        'email' => $request->email,
      ]);

      return new UserResource($user);
    }

    public function show(User $user)
    {
      return new UserResource($user);
    }

    public function update(Request $request, User $user)
    {
      // check if currently authenticated user is the owner of the book
     // if ($request->user()->id !== $kunde->user_id) {
     //   return response()->json(['error' => 'You can only edit your own books.'], 403);
     // }

      $user->update($request->only(['email']));
      if($request->input('password')){
          $user->password = Hash::make($request->input('password'));
          $user->save();
      }

      return new UserResource($user);
    }

    public function destroy(User $user)
    {
      $kunde->delete();

      return response()->json(null, 204);
    }
}
