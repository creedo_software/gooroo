<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\ArticleAttribute;

class ArticleAttributeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = new ArticleAttribute();

        $array = [];
        foreach($item->getFillable() as $k => $v){
            $array[$v] = $this->{$v};
        }
        
      return $array;
    }
}

