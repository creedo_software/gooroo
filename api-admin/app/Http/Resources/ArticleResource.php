<?php

namespace App\Http\Resources;
use App\Hersteller;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         $attributes = json_decode($this->attributes, true);
         $hersteller_name = '';
         if(!empty($attributes['ARWA4'])){
             $hersteller = Hersteller::where('id', $attributes['ARWA4'])->first();
             if(!empty($hersteller)){
                $hersteller_name = $hersteller['name'];
             }
         }
         
      return [
        'id' => $this->id,
        'number' => $this->number,
        'webshop' => $this->webshop,
        'settings' => empty($this->settings) ? [] : $this->settings,
        'attributes' => $this->attributes,
        'additional_attributes' => $this->additional_attributes,
        'media' => $this->media,
        'hersteller' => $hersteller_name
      ];
    }
}

