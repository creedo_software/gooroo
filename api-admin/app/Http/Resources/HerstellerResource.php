<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Hersteller;

class HerstellerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $kunde = new Hersteller();
        $array = [
            'id' => $this->id
        ];
        foreach($kunde->getFillable() as $k => $v){
            $array[$v] = $this->{$v};
        }
        
      return $array;
    }
}

