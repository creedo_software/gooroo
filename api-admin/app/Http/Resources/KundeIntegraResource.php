<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\KundeIntegra;

class KundeIntegraResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $kunde = new KundeIntegra();
        
        foreach($kunde->getFillable() as $k => $v){
            $array[$v] = $this->{$v};
        }
        if(empty($array['kunde_attributes'])){
            $array['kunde_attributes'] = [];
        }
        $array['magento'] = \App\Http\Resources\KundeMagentoResource::collection($this->magento);
        
        return $array;
    }
}

