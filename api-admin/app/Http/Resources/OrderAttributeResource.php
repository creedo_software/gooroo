<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\OrderAttribute;

class OrderAttributeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = new OrderAttribute();

        $array = [];
        foreach($item->getFillable() as $k => $v){
            $array[$v] = $this->{$v};
        }
        
      return $array;
    }
}

