<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\PriceKunde;

class PriceKundeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = new PriceKunde();

        $array['kd_art_nr'] = $this->kd_art_nr;

        foreach($item->getFillable() as $k => $v){
            $array[$v] = $this->{$v};
        }
        
        $array['integra'] = $this->integra;
        
      return $array;
    }
}

