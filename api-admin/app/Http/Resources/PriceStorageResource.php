<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\PriceStorage;

class PriceStorageResource extends JsonResource
{

    public function toArray($request)
    {
        $item = new PriceStorage();

        foreach($item->getFillable() as $k => $v){
            $array[$v] = $this->{$v};
        }
        
      return $array;
    }
}

