<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\SpiderPriceLatest;

class SpiderPriceLatestResource extends JsonResource
{
    
    public function formatNumber($number){
        return str_replace('.', ',', ''.round((float) $number, 2));
    }
    
    public function toArray($request)
    {
        $article = \App\Article::where([
            ['number', '=', $this->sku]
        ])->first();
        $integra_attributes = $article->getIntegraAttributes();
        
        $hersteller = '';
        if(!empty($integra_attributes['ARWA4'])){
            $hersteller = \App\Hersteller::where('id', $integra_attributes['ARWA4'])->first();
            if(!empty($hersteller)){
               $hersteller = $hersteller['name'];
            }
        }
        
        $VERPACKUNGSGRÖßE = 1;
        if($integra_attributes['VERPACKUNGSGRÖßE']){
            $VERPACKUNGSGRÖßE = $integra_attributes['VERPACKUNGSGRÖßE'];
        }
        
        if(empty($article->settings)){
            $article->settings = [];
        }
        
        $shop = \App\Shop::where('id', $this->shop_id)->first();
        $tax = 1 + floatval($shop->settings['google_shopping_tax']);

        $price_95 = \App\ArticlePrice::where([
            ['nummer', '=', $this->sku],
            ['pg', '=', 95]
        ])->first();
        
        $price_85 = \App\ArticlePrice::where([
            ['nummer', '=', $this->sku],
            ['pg', '=', 85]
        ])->first();
        if($price_95){
            $article['price'] = $price_95['price'];
            $article['price_ak'] = $price_95['price_ak'];
            
            $price_95 = $price_95->price_ak >= 0.01 ? $price_95->price_ak : $price_95->price;
        }else{
            $price_95 = 0;
        }
        if($price_85){
            $price_85 = $price_85->price_ak >= 0.01 ? $price_85->price_ak : $price_85->price;
        }else{
            $price_85 = 0;
        }
                
        $stock = $article->inStock($shop->id);
        
        $lock = null;
        if($this->google_id && $article->settings['google_id']){
            if($this->google_id == $article->settings['google_id']){
                $lock = 'locked';
            }else{
                $lock = 'unlocked';
            }
        }
        
        $pg_85_and_aufprice = !empty($article->settings['additional_price']) 
            ? ($price_85/$VERPACKUNGSGRÖßE + $article->settings['additional_price']/$tax)
            : (
                !empty($article->settings['additional_percent'])
                ? (($price_85/$VERPACKUNGSGRÖßE)*(1 + $article->settings['additional_percent']/100))
                : ''
            );
            
        $pg_85_and_aufprice_and_tax = !empty($article->settings['additional_price'])
                    ? ($tax*$price_85/$VERPACKUNGSGRÖßE + $article->settings['additional_price'])
                    : (
                        !empty($article->settings['additional_percent'])
                        ? (($tax*$price_85/$VERPACKUNGSGRÖßE)*(1 + $article->settings['additional_percent']/100))
                        : ''
                    );
        
        return [
            'id' => $this->id,
            'article' => $article,
            'columns' => [
                'A' => [
                    'letter' => 'A',
                    'name' => 'SKU',
                    'value' => $this->sku,
                    'marked' => false
                ],
                'B' => [
                    'letter' => 'B',
                    'name' => 'Image',
                    'value' => $this->parameters['image'],
                    'marked' => false
                ],
                'C' => [
                    'letter' => 'C',
                    'name' => 'Locked',
                    'value' => $lock,
                    'marked' => false
                ],
                'D' => [
                    'letter' => 'D',
                    'name' => 'EAN',
                    'value' => $integra_attributes['EAN_VERBRAUCHSEINHEIT'],
                    'marked' => false
                ],
                'E' => [
                    'letter' => 'E',
                    'name' => 'Product title',
                    'value' => $this->article_title,
                    'marked' => false
                ],
                'F' => [
                    'letter' => 'F',
                    'name' => 'Date',
                    'value' => date('Y-m-d H:i:s', strtotime($this->created_at) + 3600),
                    'marked' => false
                ],
                'G' => [
                    'letter' => 'G',
                    'name' => 'Hersteller',
                    'value' => $hersteller,
                    'marked' => false
                ],
                'H' => [
                    'letter' => 'H',
                    'name' => 'Google title',
                    'value' => $this->title,
                    'marked' => false
                ],
                'I' => [
                    'letter' => 'I',
                    'name' => 'Google link',
                    'value' => $this->link,
                    'marked' => false
                ],
                'J' => [
                    'letter' => 'J',
                    'name' => 'Google ID',
                    'value' => !empty($article->settings['google_id']) ? $article->settings['google_id'] : '',
                    'marked' => false
                ],
                'K' => [
                    'letter' => 'K',
                    'name' => 'Bestand 0001',
                    'value' => $stock['lager_0001'],
                    'marked' => false
                ],
                'L' => [
                    'letter' => 'L',
                    'name' => 'Bestand 0001+7000',
                    'value' => $stock['total'],
                    'marked' => false
                ],
                'M' => [
                    'letter' => 'M',
                    'name' => 'Aufpreis, Netto',
                    'value' => !empty($article->settings['additional_price'])
                                ? $this->formatNumber($article->settings['additional_price']).'€' 
                                : (
                                    !empty($article->settings['additional_percent'])
                                    ? $this->formatNumber($article->settings['additional_percent']).'%'
                                    : ''
                                ),
                    'marked' => false
                ],
                'N' => [
                    'letter' => 'N',
                    'name' => 'Preisänderung Google Preis, €',
                    'value' => isset($article->settings['min_difference'])
                                ? $this->formatNumber($article->settings['min_difference']) 
                                : '',
                    'marked' => false
                ],
                'O' => [
                    'letter' => 'O',
                    'name' => 'UVP price, € Brutto',
                    'value' => isset($article->settings['uvp_price'])
                                ? $this->formatNumber($article->settings['uvp_price'])
                                : '',
                    'marked' => false
                ],
                'P' => [
                    'letter' => 'P',
                    'name' => 'Fixed price, € Brutto',
                    'value' => isset($article->settings['fixed_price'])
                                ? $this->formatNumber($article->settings['fixed_price'])
                                : '',
                    'marked' => abs($article->getPrice($shop, false) - (!empty($article->settings['fixed_price'])
                                ? $article->settings['fixed_price']
                                : 0)) < 0.01
                ],
                'Q' => [
                    'letter' => 'Q',
                    'name' => 'Fixed price (Special), € Brutto',
                    'value' => isset($article->settings['fixed_price_ak'])
                                ? $this->formatNumber($article->settings['fixed_price_ak'])
                                : '',
                    'marked' => false
                ],
                'R' => [
                    'letter' => 'R',
                    'name' => 'Herstelleruntergrenze, €',
                    'value' => isset($article->settings['manufacturer_min_price'])
                                ? $this->formatNumber($article->settings['manufacturer_min_price']) 
                                : '',
                    'marked' => false
                ],
                'S' => [
                    'letter' => 'S',
                    'name' => 'PG85 €, Netto',
                    'value' => $this->formatNumber($price_85),
                    'marked' => false
                ],
                'T' => [
                    'letter' => 'T',
                    'name' => 'PG85 per item €, Netto',
                    'value' => $this->formatNumber($price_85/$VERPACKUNGSGRÖßE),
                    'marked' => false
                ],
                'U' => [
                    'letter' => 'U',
                    'name' => 'PG85 per item + Aufpreis €, Netto',
                    'value' => is_numeric($pg_85_and_aufprice) ? $this->formatNumber($pg_85_and_aufprice) : '',
                    'marked' => is_numeric($pg_85_and_aufprice) && abs($article->getPrice($shop) - $pg_85_and_aufprice) < 0.01
                ],
                'V' => [
                    'letter' => 'V',
                    'name' => 'PG85 per item + tax €, Brutto',
                    'value' => $this->formatNumber($tax*$price_85/$VERPACKUNGSGRÖßE),
                    'marked' => false
                ],
                'W' => [
                    'letter' => 'W',
                    'name' => 'PG85 per item + tax + Aufpreis €, Brutto',
                    'value' => is_numeric($pg_85_and_aufprice_and_tax) ? $this->formatNumber($pg_85_and_aufprice_and_tax) : '',
                    'marked' => is_numeric($pg_85_and_aufprice_and_tax) && abs($article->getPrice($shop, false) - $pg_85_and_aufprice_and_tax) < 0.01
                ],
                'X' => [
                    'letter' => 'X',
                    'name' => 'PG95 €, Netto',
                    'value' => $this->formatNumber($price_95),
                    'marked' => false
                ],
                'Y' => [
                    'letter' => 'Y',
                    'name' => 'PG95 per item €, Netto',
                    'value' => $this->formatNumber($price_95/$VERPACKUNGSGRÖßE),
                    'marked' => abs($article->getPrice($shop) - $price_95/$VERPACKUNGSGRÖßE) < 0.01
                ],
                'Z' => [
                    'letter' => 'Z',
                    'name' => 'PG95 per item + tax €, Brutto',
                    'value' => $this->formatNumber($tax*$price_95/$VERPACKUNGSGRÖßE),
                    'marked' => false
                ],
                'AA' => [
                    'letter' => 'AA',
                    'name' => 'Google price €, Brutto',
                    'value' => $this->formatNumber($this->price),
                    'marked' => abs($article->getPrice($shop, false) - $this->price) < 0.01
                ],
                'AB' => [
                    'letter' => 'AB',
                    'name' => 'Google price - Preisänderung €, Brutto',
                    'value' => $this->formatNumber($this->price - (isset($article->settings['min_difference']) ? (float) $article->settings['min_difference'] : 0)),
                    'marked' => false
                ],
                'AC' => [
                    'letter' => 'AC',
                    'name' => 'Google/EK%',
                    'value' => $price_85 ? $this->formatNumber(($this->price/($tax*$price_85/$VERPACKUNGSGRÖßE) - 1)*100) : '',                    
'marked' => false
                ],
                'AD' => [
                    'letter' => 'AD',
                    'name' => 'UVP - Google preis, €',
                    'value' => !empty($article->settings['uvp_price']) ? $this->formatNumber((float) $article->settings['uvp_price'] - $this->price) : '',
                    'marked' => false
                ],
                'AE' => [
                    'letter' => 'AE',
                    'name' => 'Fixpreis - Google preis, €',
                    'value' => !empty($article->settings['fixed_price']) ? $this->formatNumber((float) $article->settings['fixed_price'] - $this->price) : '',
                    'marked' => false
                ],
                'AG' => [
                    'letter' => 'AG',
                    'name' => 'Herstelleruntergrenze - Google preis, €',
                    'value' => !empty($article->settings['manufacturer_min_price']) ? $this->formatNumber((float) $article->settings['manufacturer_min_price'] - $this->price) : '',
                    'marked' => false
                ],
                'AH' => [
                    'letter' => 'AH',
                    'name' => 'Result €, Netto',
                    'value' => $this->formatNumber($article->getPrice($shop)),
                    'marked' => true
                ],
                'AI' => [
                    'letter' => 'AI',
                    'name' => 'Result €, Brutto',
                    'value' => $this->formatNumber($article->getPrice($shop, false)),
                    'marked' => true
                ],
                'AJ' => [
                    'letter' => 'AJ',
                    'name' => 'Special €, Netto',
                    'value' => $this->formatNumber($article->getSpecialPrice($shop)),
                    'marked' => true
                ],
                'AK' => [
                    'letter' => 'AK',
                    'name' => 'Special €, Brutto',
                    'value' => $this->formatNumber($article->getSpecialPrice($shop, false)),
                    'marked' => true
                ],
                'AL' => [
                    'letter' => 'AL',
                    'name' => 'Notes',
                    'value' => !empty($article->settings['notes']) ? $article->settings['notes'] : '',
                    'marked' => false
                ],
            ]
        ];
    }
}

