<?php

namespace App\Jobs;

use App\Migration;
use App\Article;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadAllArticlesAdditionalFromIntegra extends JobIntegra
{

    public function __construct($offset, $count)
    {
		if($offset == 4100){
			$count = 80;
		}
		//4180 - 4185 with bugs
		$this->offset = $offset;
		$this->count = $count;	
    }

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $articles = $this->callSOAP(
				'IntegraWWS.RequestQuery', 
				[
					'Token' => $this->AuthToken,
					'QueryRequest' => [
						'QID' => 100101,
						'Parameters' => [],
						'Offset' => $this->offset,
						'Count' => $this->count,
						'RTFConvert' => ''
					]
				],
				\App\Log::LOAD_ADDITIONAL_ARTICLES_FROM_INTEGRA
			);
			
			if(!empty($articles->Rows)){
				foreach($articles->Rows as $xml){
					$countSaved++;
					$this->offset++;
					
					$attributes = [];
					foreach($articles->ColumnDescription->Cells as $k => $v){
						$attributes[$v] = $xml->Cells[$k];
					}
					
					$number = $xml->Cells[array_search('ArtikelNumber', $articles->ColumnDescription->Cells)];
					
					Article::firstOrCreate(['number' => $number], ['attributes' => '{}', 'additional_attributes' => '{}']);

					Article::updateOrCreate(
						['number' => $number],
						[
							'additional_attributes' => json_encode($attributes),
							'webshop' => !empty($attributes['WEB_SHOP']) ? $attributes['WEB_SHOP'] : ''
						]
					);           
				}
			}
		}catch(Exception $e){
			var_dump($e);
		}
		if($this->offset == 4180){
			$this->offset = 4185;
		}
        		
		if($countSaved){
            Migration::next_load_all_articles_additional_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_all_articles_additional_from_integra();
	   }
    }
	
	
	
}
