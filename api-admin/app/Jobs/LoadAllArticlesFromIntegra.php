<?php

namespace App\Jobs;

use App\Migration;
use App\Article;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadAllArticlesFromIntegra extends JobIntegra
{

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $articles = $this->callSOAP(
				'IntegraWWS.RequestQuery', 
				[
					'Token' => $this->AuthToken,
					'QueryRequest' => [
						'QID' => 100100,
						'Parameters' => [],
						'Offset' => $this->offset,
						'Count' => $this->count,
						'RTFConvert' => ''
					]
				],
				\App\Log::LOAD_ARTICLES_FROM_INTEGRA
			);

			if(!empty($articles->Rows)){
				foreach($articles->Rows as $xml){
					$countSaved++;
					$this->offset++;
					
					$attributes = [];
					foreach($articles->ColumnDescription->Cells as $k => $v){
						$attributes[$v] = $xml->Cells[$k];
					}
					
					$number = $xml->Cells[array_search('ArtikelNumber', $articles->ColumnDescription->Cells)];
					
					Article::firstOrCreate(['number' => $number], ['attributes' => '{}', 'additional_attributes' => '{}']);

					Article::updateOrCreate(
						['number' => $number],
						['attributes' => json_encode($attributes)]
					);           
				}
			}
		}catch(Exception $e){
			var_dump($e);
		}
		
		var_dump('COUNT SAVED ', $countSaved);
		if($countSaved){
			Migration::next_load_all_articles_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_all_articles_from_integra();
	   }
    }
	
	
}
