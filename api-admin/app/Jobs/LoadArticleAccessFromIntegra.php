<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleAccess;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadArticleAccessFromIntegra extends JobIntegra
{

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $countSaved = ArticleAccess::importFromIntegra($this->offset, $this->count);
		}catch(Exception $e){
			var_dump($e);
		}
		
		if($countSaved){
            Migration::next_load_article_access_from_integra([
                'offset' => $this->offset + $countSaved, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_article_access_from_integra();
	   }
    }
	
	
}
