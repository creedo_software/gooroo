<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleDiscount;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class LoadArticleDiscountsFromCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $storage_id;

    public function __construct($storage_id)
    {
        $this->storage_id = $storage_id;
    }
	
    public function handle()
    {			
		try{
	        $filename = Storage::disk('tmp')->path($this->storage_id);
            
            $file = fopen($filename, "r");
            ArticleDiscount::truncate();
            while ( ($data = fgetcsv($file, 200, ";")) !== FALSE ) {
                if(count($data) != 22){
                    continue;
                }
                $values = [
                    'zabtnr' => $data[0],
                    'zaidnr' => (int) $data[1],
                    'zaabta' => $data[2],
                    'zatyp' => $data[3],
                    'zarbasis' => $data[4], 
                    'zainex' => $data[5], 
                    'zarang' => $data[6],
            		'zaabme' => $data[7], 
                    'zaartn' => $data[8], 
                    'zavar' => $data[9], 
                    'zawg' => $data[10], 
                    'zaerzc' => $data[11], 
                    'zawa1' => $data[12], 
                    'zawa2' => $data[13], 
                    'zawa3' => $data[14], 
            		'zawa4' => $data[15], 
                    'zasort' => $data[16], 
                    'zageba' => $data[17], 
                    'zagebi' => $data[18], 
                    'zaabfa' => $data[19], 
                    'zawert_neu' => $data[20], 
                    'zakzab' => $data[21]
                ];
                $values['zaidnr_zaabme_zatyp_zageba'] = $values['zaidnr'].'_'.$values['zaabme'].'_'.$values['zatyp'].'_'.$values['zageba'];
                var_dump($values);
                ArticleDiscount::updateOrCreate(
                    ['zaidnr_zaabme_zatyp_zageba' => $values['zaidnr_zaabme_zatyp_zageba']],
                    $values
                );  
            }
            Storage::disk('tmp')->delete($this->storage_id);
		}catch(Exception $e){
			var_dump($e);
		}
		
		Migration::finish_load_article_discounts_from_csv();
    }
	
	
}
