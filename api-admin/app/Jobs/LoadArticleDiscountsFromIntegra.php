<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleDiscount;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadArticleDiscountsFromIntegra extends JobIntegra
{

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $items = $this->callSOAP(
				'IntegraWWS.RequestQuery', 
				[
					'Token' => $this->AuthToken,
					'QueryRequest' => [
						'QID' => 100400,
						'Parameters' => [],
						'Offset' => $this->offset,
						'Count' => $this->count,
						'RTFConvert' => ''
					]
				],
				'Load Article Discounts from Integra'
			);

			if(!empty($items->Rows)){
				foreach($items->Rows as $xml){
					$countSaved++;
					$this->offset++;
					
					$values = [];
					foreach([
                        "ZAIDNR", "ZAABTA", "ZABTNR", "ZATYP", "ZARBASIS", 
                        "ZAINEX", "ZARANG", "ZAABME", "ZAARTN", "ZAVAR", 
                        "ZAWG", "ZAERZC", "ZAWA1", "ZAWA2", "ZAWA3", "ZAWA4", 
                        "ZASORT", "ZAGEBA", "ZAGEBI", "ZAABFA", "ZAWERT_NEU", 
                        "ZAKZAB",
					] as $key){
						$values[strtolower($key)] = $xml->Cells[array_search($key, $items->ColumnDescription->Cells)];
					}
                    $values['zaidnr'] = (int) $values['zaidnr'];
					$values['zaidnr_zaabme_zatyp_zageba'] = $values['zaidnr'].'_'.$values['zaabme'].'_'.$values['zatyp'].'_'.$values['zageba'];
					ArticleDiscount::updateOrCreate(
						['zaidnr_zaabme_zatyp_zageba' => $values['zaidnr_zaabme_zatyp_zageba']],
						$values
					);           
				}
			}
		}catch(Exception $e){
			var_dump($e);
		}
		
		var_dump('COUNT SAVED ', $countSaved);
		if($countSaved){
			Migration::next_load_article_discounts_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_article_discounts_from_integra();
	   }
    }
	
	
}
