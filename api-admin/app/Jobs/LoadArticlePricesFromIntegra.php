<?php

namespace App\Jobs;

use App\Migration;
use App\ArticlePriceDraft;
use App\Article;

use Illuminate\Support\Facades\Schema;
use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class LoadArticlePricesFromIntegra extends JobIntegra
{
	
    public function process()
    {
		$countSaved = 0;
		//\Log::info('Offset: '.$this->offset.' Count: '.$this->count);
		
		if($this->offset == 0){
			DB::statement('CREATE TABLE IF NOT EXISTS article_prices_draft LIKE article_prices');
		}
		
		try{
	        $items = $this->callSOAP(
				'IntegraWWS.RequestQuery', 
				[
					'Token' => $this->AuthToken,
					'QueryRequest' => [
						'QID' => 100200,
						'Parameters' => [],
						'Offset' => $this->offset,
						'Count' => $this->count,
						'RTFConvert' => ''
					]
				],
				\App\Log::LOAD_ARTICLE_PRICES_FROM_INTEGRA
			);

			if(!empty($items->Rows)){
				foreach($items->Rows as $xml){
					$countSaved++;
					$this->offset++;
					
					$values = [];
					foreach(["BS", "NUMMER", "PRVAR", "PG", "NETTO_BRUTTO", "PREIS1", "SATZSTATUS"] as $key){
						$values[strtolower($key)] = $xml->Cells[array_search($key, $items->ColumnDescription->Cells)];
					}
					$values['nummber'] = (int) $values['nummer'];
					//\Log::info('Number/SKU: '.$values['nummer']);
					
					$values['pg'] = strtolower($values['pg']);
					$is_ak = false !== strpos($values['pg'], 'ak');
					$values['pg'] = str_replace('ak', '', $values['pg']);
					
					$values['nummer_pg'] = $values['nummer'].'_'.$values['pg'];
					
			        $exists = ArticlePriceDraft::where('nummer_pg', $values['nummer_pg'])->first();
					if($exists){
						if($is_ak){
							$exists->price_ak = $values['preis1'];
						}else{
							$exists->price = $values['preis1'];
						}
						$exists->save();
					}else{
						if($is_ak){
							$values['price_ak'] = $values['preis1'];
						}else{
							$values['price'] = $values['preis1'];
						}
						ArticlePriceDraft::create($values);
					}
					
					
				}
			}
		}catch(Exception $e){
			var_dump($e);
			Migration::finish_load_article_prices_from_integra();
			return;
		}
		
		//\Log::info('Total saved: '.$countSaved);

		if($countSaved){
			Migration::next_load_article_prices_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			if(
				Schema::hasTable('article_prices_backup') 
				&& Schema::hasTable('article_prices_draft')
				&& Schema::hasTable('article_prices')
			){
				Schema::drop('article_prices_backup');
			}
			Schema::rename('article_prices', 'article_prices_backup');
			Schema::rename('article_prices_draft', 'article_prices');
			
			Migration::finish_load_article_prices_from_integra();
	   }
    }
	
	
}
