<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleStock;
use App\Article;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadArticleStocksFromIntegra extends JobIntegra
{

    public function process()
    {
		$articles = Article::where('id', '>=', $this->offset)->orderBy('id', 'asc')->paginate($this->count);

		$ids = [];
		foreach($articles as $article){
			$this->offset = $article->id;
			$ids[] = $article->number;
		}
		
		try{
			$stocks = $this->callSOAP('IntegraWWS.GetArtikelVerfuegbarkeitNachLager', [
				'Token' => $this->AuthToken,
				'ArtikelNummern' => $ids,
				'LagerList' => ['0001', '7000']
			], \App\Log::LOAD_STOCKS_FROM_INTEGRA);
		}catch(Exception $e){
			var_dump($e);
		}
		var_dump('STOCKS', $stocks);
		foreach($stocks as $stock){
			if(empty($stock->ArtikelNummer)){
				continue;
			}
			
			ArticleStock::updateOrCreate(
				['sku_lager' => $stock->ArtikelNummer.'_'.$stock->Lager],
				[
					'details' => json_encode($stock), 
					'quantity' => $stock->Bestand, 
					'sku' => $stock->ArtikelNummer,
					'lager' => $stock->Lager
				]
			);
		}
		
		if(count($articles) >= $this->count){
            Migration::next_load_article_stocks_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_article_stocks_from_integra();
	   }
    }
	
	
}
