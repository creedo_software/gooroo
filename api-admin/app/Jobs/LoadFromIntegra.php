<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Response\ConnectionInfo;
use App\Soap\Response\AuthToken;
use Illuminate\Support\Facades\Cache;

class JobIntegra implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $AuthToken;
    protected $soapWrapper;
	
	protected $offset; 
	protected $count;
    protected $max;
    
    public function __construct($offset, $count, $max = null)
    {
		$this->offset = $offset;
		$this->count = $count;	
        $this->max = $max;
        
        var_dump('OFFSET: '.$this->offset);
        var_dump('COUNT: '.$this->count);
    }
        
    public function handle()
    {
        if(!empty($this->max) && $this->max >= $this->offset){
            return;
        }
        
		$this->soapWrapper = new SoapWrapper();
        $integra_conf = \Config::get('integra');
        
        foreach(['IntegraSessionManager', 'IntegraWWS', 'IntegraCRM'] as $type){
            $this->soapWrapper->add($type, function ($service)  use ($type, $integra_conf) {
                $service
                ->wsdl($integra_conf['host'].'/wsdl/' . $type)
                ->options([
                ])
                ->trace(true);
            });
        }

        $this->AuthToken = $this->getAuthToken();
        
        $this->process();
	}
    
    public function getAuthToken(){
		$token = Cache::get('INTEGRA_AUTH_TOKEN');
		if($token){
			return $token;
		}
		
		return $this->login();
	}
    
    protected function callSOAP($call, $params, $name){
        $result = $this->soapWrapper->call($call, $params);
        
        list($soap_name, $soap_function) = explode('.', $call, 2);
        
        \App\Log::create([
            'name' => $name,
            'type' => 'SOAP_CALL',
            'description' => $name,
            'attributes' => json_encode([
                'request' => $this->soapWrapper->client($soap_name, function ($client) {
                    return $client->getLastRequest();
                }),
                'params' => $params,
                'soap_name' => $soap_name,
                'soap_function' => $soap_function
            ]),
            'log' => $this->soapWrapper->client($soap_name, function ($client) {
                return $client->getLastResponse();
            }),            
        ]);
        
        return $result;
    }
    
    protected function callRaw($xml, $name, $soap_name, $soap_function){
        $integra_conf = \Config::get('integra');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $integra_conf['host'].'/soap/'.$soap_name);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: text/xml',
            'SOAPACTION: urn:'.$soap_name.'#'.$soap_function
        ]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec($ch);
        curl_close($ch);
        
        \App\Log::create([
            'name' => $name,
            'type' => 'SOAP_CALL',
            'description' => $name,
            'attributes' => json_encode([
                'request' => $xml,
                'soap_name' => $soap_name,
                'soap_function' => $soap_function
            ]),
            'log' => $result,            
        ]);
        
        return $result;
    }
	
	protected function login(){
        $response = $this->soapWrapper->call('IntegraSessionManager.RequestLogin', [
            'User' => 'webservice', 
            'Password' => 'webservice', 
            'Mandant' => '1', 
            'BetrSt' => '1',
        ]);

        $this->AuthToken = $response->AuthToken;
    }
}