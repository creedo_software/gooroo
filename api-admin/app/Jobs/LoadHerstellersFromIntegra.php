<?php

namespace App\Jobs;

use App\Migration;
use App\Hersteller;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadHerstellersFromIntegra extends JobIntegra
{

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $herstellers = $this->callSOAP(
				'IntegraWWS.RequestQuery', 
				[
					'Token' => $this->AuthToken,
					'QueryRequest' => [
						'QID' => 100110,
						'Parameters' => [],
						'Offset' => $this->offset,
						'Count' => $this->count,
						'RTFConvert' => ''
					]
				],
				\App\Log::LOAD_HERSTELLERS_FROM_INTEGRA
			);

			if(!empty($herstellers->Rows)){
				foreach($herstellers->Rows as $xml){
					$countSaved++;
					$this->offset++;
					
					$id = $xml->Cells[array_search('HERSTELLER', $herstellers->ColumnDescription->Cells)];
					$name = $xml->Cells[array_search('NAME', $herstellers->ColumnDescription->Cells)];
					var_dump($id, $name);
					Hersteller::updateOrCreate(
						['id' => $id],
						['name' => $name]
					);           
				}
			}
		}catch(Exception $e){
			var_dump($e);
		}
		
		var_dump('COUNT SAVED ', $countSaved);
		if($countSaved){
            Migration::next_load_herstellers_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_herstellers_from_integra();
	   }
    }
	
	
}
