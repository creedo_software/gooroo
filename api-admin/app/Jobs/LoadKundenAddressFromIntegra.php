<?php

namespace App\Jobs;

use App\Migration;
use App\KundeIntegra;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadKundenAddressFromIntegra extends JobIntegra
{

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $countSaved = \App\KundeAddress::importFromIntegra($this->offset, $this->count);
		}catch(Exception $e){
			var_dump($e);
		}
		
		if($countSaved){
            Migration::next_load_kunden_address_from_integra([
                'offset' => $this->offset + $countSaved, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_kunden_address_from_integra();
	   }
    }
	
	
}
