<?php

namespace App\Jobs;

use App\KundeAttachment;
use App\Migration;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadKundenAttachmentsFromFTP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $credentials;
	protected $directory;
    protected $offset;
    protected $limit;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($credentials, $directory, $offset = 0, $limit = 100)
    {
		$this->credentials = $credentials;
		$this->directory = $directory;
        $this->offset = $offset;
        $this->limit = $limit;
    }
	
	/**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $conn = ftp_connect($this->credentials['host']); 
		
	    if (ftp_login($conn, $this->credentials['username'], $this->credentials['password'])) 
	    { 
			ftp_pasv($conn, true);  
			
			$list = ftp_nlist($conn, $this->directory);
			$total = count($list);
            
            $list = array_slice($list, $this->offset, $this->limit);
            if(empty($list)){
                var_dump('EMPTY FILES LIST');
            }
            
			foreach ($list as $filename) {
                $filename = str_replace($this->directory . '/', '', $filename);
                var_dump('FILENAME', $filename);
                if(in_array($filename, ['.', '..'])){
                    continue;
                }
                var_dump('FILENAME: '.$filename);
                $exists = KundeAttachment::where('filename', '=', $filename)->first();
                if($exists){
                    var_dump('EXISTS, SKIP');
                    continue;
                }
                
                $attachment = new \App\KundeAttachment();
        		$attachment->storage_id = $attachment->generateStorageId();
                $parts = explode('_', preg_replace('/\.[a-z]+$/i', '', $filename));
                $date_parts = explode('.', $parts[2]);
                var_dump($date_parts);
                $attachment->year = $date_parts[2];
                $attachment->month = $date_parts[1];
                $attachment->day = $date_parts[0];
                $attachment->filename = $filename;
                $attachment->path = $attachment->year . '/' . $attachment->month . '/' . $attachment->day;
                $attachment->integra_kunde_id = (int) $parts[0];
                $attachment->total = (float) str_replace(',', '.', $parts[4]);
                 
                if((int) $attachment->year < 2020){
                    var_dump('SKIP: ', $filename);
                    continue;
                }
                
				if (ftp_get($conn, $attachment->storage_id, $this->directory.'/'.$filename, FTP_BINARY)) {
					//024233_RECHNUNG_05.12.2016_6107321_115,83.pdf
                   
					$full_path = public_path().'/attachments/'.$attachment->path;
					if(!file_exists($full_path)){
						mkdir($full_path, 777, true);
					}
					$filepath = $full_path.'/'.$attachment->storage_id;
                    var_dump("\t SAVED TO: ".$filepath);
					rename($attachment->storage_id, $filepath);

	                if(file_exists($attachment->storage_id)){
						unlink($attachment->storage_id);
					}

					$attachment->save();
				}
			}
            
            var_dump('TOTAL: ', $total);
            var_dump('OFFSET: ', $this->offset);
            if($total > $this->offset){
                var_dump('STARTING NEW');
    			LoadKundenAttachmentsFromFTP::dispatch($this->credentials, $this->directory, $this->offset + $this->limit, $this->limit)
                    ->onQueue('load_kunden_attachments_from_ftp');
    		}else{
                var_dump('FINISH ALL');
    			Migration::finish_load_kunden_attachments_from_ftp();
    	   }
       }else{
           
       }
    }
}
