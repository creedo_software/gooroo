<?php

namespace App\Jobs;

use App\KundeMagento;
use App\Migration;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadKundenFromMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset;
	protected $limit;
    protected $shop_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id, $offset, $limit)
    {
		$this->offset = $offset;
		$this->limit = $limit;
        $this->shop_id = $shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $imported = KundeMagento::importFromMagento($this->shop_id, $this->offset, $this->limit);
		if($imported){
			$this->offset++;
            
            Migration::{'next_load_kunden_from_magento_shop_'.$this->shop_id}([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::{'finish_load_kunden_from_magento_shop_'.$this->shop_id}();
	   }
    }
}
