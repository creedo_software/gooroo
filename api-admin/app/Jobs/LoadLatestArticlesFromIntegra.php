<?php

namespace App\Jobs;

use SimpleXMLElement;

use App\Migration;
use App\Article;
use App\Shop;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadLatestArticlesFromIntegra extends JobIntegra
{

	protected $offset; 
	protected $count;
	protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = 5;	
		$this->shop_id = $shop_id;
    }

    public function process()
    {
		$shop = Shop::where('id', $this->shop_id)->first();
		$countSaved = 0;
		
		try{
			$date = date('d.m.Y', strtotime('-2 days'));
			$offset = $this->offset;
			$count = $this->count;
			
			$xml = <<<XML
	 <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	      xmlns:urn="urn:IntegraWWS" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
	 <soapenv:Header/>
	 <soapenv:Body>
	    <urn:RequestQuery>
	        <Token xsi:type="ns2:AuthToken">
	            <SessionID xsi:type="xsd:string">{$this->AuthToken->SessionID}</SessionID>
	        </Token>
	        <QueryRequest xsi:type="ns2:QueryRequest">
	            <QID xsi:type="xsd:unsignedInt">100100</QID>
				<Offset xsi:type="xsd:int">$offset</Offset>
				<Count xsi:type="xsd:int">$count</Count>
				<RTFConvert xsi:type="ns2:RTFConvertOptions"></RTFConvert>
	            <Parameters>
	                
	                <Item>
	                    <Name>PARAM1</Name>
	                    <Value>$date</Value>
	                </Item>
	            </Parameters>
	        </QueryRequest>
	    </urn:RequestQuery>
	 </soapenv:Body>
	 </soapenv:Envelope>
XML;

			

			var_dump($xml);
			$result = $this->callRaw(
				$xml, 
				\App\Log::LOAD_LATEST_ARTICLES_FROM_INTEGRA, 
				'IntegraWWS', 
				'RequestQuery'
			);
			$articles = simplexml_load_string($result);
			var_dump('PARSED', 
			$articles->xpath('//Rows')
		
		);
			
			
			
			
			foreach((array) $articles->xpath('//Rows/item/Cells') as $xmlItem){
				$countSaved++;
				
				$attributes = [];
				$ColumnDescription = (array) $articles->xpath('//ColumnDescription')[0]->Cells->item;
				foreach($ColumnDescription as $k => $v){
					$value = (array) $xmlItem->item[$k];
					$attributes[$v] = $value ?trim($value[0], ' "') : '';
				}
				
				$number = $attributes['NUMMER'];
				
				Article::firstOrCreate(['number' => $number], ['attributes' => '{}', 'additional_attributes' => '{}']);
				var_dump($number);
				Article::updateOrCreate(
					['number' => $number],
					['attributes' => json_encode($attributes)]
				);    
				
				$article = Article::where('number', $number)
					->whereIn('webshop', $shop->getWebshops())
					->leftJoin('article_prices', function ($join) use ($shop) {
		                $join->on('article_prices.nummer', '=', 'articles.number')
		                    ->where('article_prices.pg', '=', $shop->pg);
		            })
				->first();
				if($article){
					$result = $article->exportToMagento($this->shop_id);  
				}  				
			}
		}catch(Exception $e){
			var_dump($e);
		}
		
		var_dump('COUNT SAVED ', $countSaved);
		if($countSaved){
			Migration::{'next_load_latest_articles_from_integra_shop_'.$this->shop_id}([
                'offset' => $this->offset + $this->count, 'limit' => $this->count
            ]);
		}else{
			Migration::{'finish_load_latest_articles_from_integra_shop_'.$this->shop_id}();
			
			\App\Jobs\SyncArticlesWithMagento::dispatch($this->shop_id, 0, 100);
	   }
    }
	
	
}
