<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadMediaFromFTP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $file;
    protected $shop_id;

    public function __construct($shop_id, $file)
    {
        $this->shop_id = $shop_id;
		$this->file = $file;
    }
	
	public function handle()
    {
        $shop = \App\Shop::where('id', $this->shop_id)->first();
        
        $media = new \App\ArticleMedia();
        if($media->loadFromFTP($this->file, $shop)){
            $media->exportToMagento($this->shop_id);
        }
        
        \App\Job::finish('load_media_from_ftp');
    }
}
