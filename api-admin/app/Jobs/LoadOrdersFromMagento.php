<?php

namespace App\Jobs;

use App\Order;
use App\Migration;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadOrdersFromMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset;
	protected $limit;
    protected $shop_id;

    public function __construct($shop_id, $offset, $limit)
    {
		$this->offset = $offset;
		$this->limit = $limit;
        $this->shop_id = $shop_id;
    }

    public function handle()
    {
        var_dump('OFFSET: ', $this->offset, 'LIMIT: ', $this->limit);
        $imported = Order::importFromMagento($this->shop_id, $this->offset, $this->limit);
		
        if($imported){
			$this->offset++;
            
            Migration::{'next_load_orders_from_magento_shop_'.$this->shop_id}([
                'offset' => $this->offset, 'limit' => $this->limit
            ]);
		}else{
			Migration::{'finish_load_orders_from_magento_shop_'.$this->shop_id}();
	   }
    }
}
