<?php

namespace App\Jobs;

use App\Migration;
use App\PriceKunde;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadPriceKundenFromIntegra extends JobIntegra
{

    public function process()
    {
		$countSaved = 0;
		
		try{
	        $items = $this->callSOAP(
				'IntegraWWS.RequestQuery', 
				[
					'Token' => $this->AuthToken,
					'QueryRequest' => [
						'QID' => 100300,
						'Parameters' => [],
						'Offset' => $this->offset,
						'Count' => $this->count,
						'RTFConvert' => ''
					]
				],
				'Load Price Kunden from Integra'
			);

			if(!empty($items->Rows)){
				foreach($items->Rows as $xml){
					$countSaved++;
					$this->offset++;
					
					$values = [];
					foreach(["BS", "KD_NR", "ART_NR", "PRVAR", "SPKMS", "RABATTFÄHIG", "PREIS2", "SATZSTATUS"] as $key){
						$values[strtolower(str_replace('Ä', 'a', $key))] = $xml->Cells[array_search($key, $items->ColumnDescription->Cells)];
					} 
					$values['kd_art_nr'] = $values['kd_nr'].'-'.$values['art_nr'];
					
					PriceKunde::updateOrCreate(
						['kd_art_nr' => $values['kd_art_nr']],
						$values
					);           
				}
			}
		}catch(Exception $e){
			var_dump($e);
		}
		
		var_dump('COUNT SAVED ', $countSaved);
		if($countSaved){
            Migration::next_load_price_kunden_from_integra([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::finish_load_price_kunden_from_integra();
	   }
    }
	
	
}
