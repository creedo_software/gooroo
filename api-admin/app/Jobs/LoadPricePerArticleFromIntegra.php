<?php

namespace App\Jobs;

use App\Migration;
use App\Article;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadPricePerArticleFromIntegra extends JobIntegra
{

    public function process()
    {		
		$articles = Article::where('id', '>', $this->offset)
			->where('price', '<', 0.01)
			->orderBy('id', 'asc')
			->paginate($this->count);
		var_dump('OFFSET', $this->offset, $this->count);
		var_dump('COUNT', count($articles));
		if(!count($articles)){
			Migration::finish_load_price_per_article_from_integra();
			return;
		}
		
		$prices = [];
		$params = [];
		foreach($articles as $article){
			$this->offset = $article->id;
			$params[] = [
				'ArtikelNummer' => $article->number,
				'Menge' => 1,
				'Waehrung' => 'EUR',
				'Datum' => date('Y-m-d'),
				'KundenNummer' => '599000',//589999 
				'Brutto' => 'True',
				'Typ' => 'aptVerkauf',//aptVerkauf, aptEinkauf
				'Preisgruppe' => '01',
				'Details' => 'True',
			];
		}
		
		try{
			$prices = $this->soapWrapper->call('IntegraWWS.GetArtikelPreisMitRabatten', [
				'Token' => $this->AuthToken,
				'Artikel' => $params
			]);
		}catch(Exception $e){
			var_dump($e);
		}
		var_dump('PRICES', $prices);
		foreach($prices as $price){
			var_dump($price);
			if(empty($price->ArtikelNummer)){
				continue;
			}
			Article::updateOrCreate(
				['number' => (int) $price->ArtikelNummer],
				['price_details' => json_encode($price), 'price' => $price->Listenpreis]
			);
		}
		
		LoadPricePerArticleFromIntegra::dispatch($this->offset, $this->count);
    }
	
	
}
