<?php

namespace App\Jobs;

use App\Migration;
use App\Article;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadPricesFromGoogle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $webshop;
	protected $offset; 
	protected $count;
	protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->shop_id = $shop_id;
		$this->offset = $offset;
		$this->count = 3;	
    }

    public function handle()
    {
		$countSaved = 0;
		$lastSKU = null;
		
		try{
			$shop = \App\Shop::where('id', $this->shop_id)->first();
			
	        $articles = Article::where([
                ['number', '>', $this->offset],
            ])
			->select('articles.*')
		    ->whereRaw('JSON_EXTRACT(articles.settings, "$.google_id") is not null')
			->whereIn('webshop', $shop->getWebshops())
            ->orderBy('number', 'asc')
            ->paginate($this->count);

			foreach($articles as $article){	 
				var_dump('NUMBER', $article->number);           
				$article->loadPricesFromGoogle($this->shop_id);
				$countSaved++;
				$lastSKU = $article->number;
			}
		}catch(Exception $e){
			var_dump($e);
		}
		
		var_dump('COUNT SAVED ', $countSaved);
		if($countSaved){
			LoadPricesFromGoogle::dispatch($this->shop_id, $lastSKU, $this->count)
				->onQueue('load_prices_from_google_shop_'.$this->shop_id);
		}else{
			Migration::{'finish_load_prices_from_google_shop_'.$this->shop_id}();
	   }
    }
	
	
}
