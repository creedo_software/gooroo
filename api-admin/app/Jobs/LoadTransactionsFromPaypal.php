<?php

namespace App\Jobs;

use App\Migration;
//use App\ArticlePriceDraft;
//use App\Article;

use Illuminate\Support\Facades\Schema;
use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

use App\PayPal\Sync;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class LoadTransactionsFromPaypal extends JobIntegra
{

	protected $offset; 
	protected $count;
	protected $shop_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = $count;	
		$this->shop_id = $shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		//parent::handle();
		$countSaved = 0;
		\Log::info('Offset: '.$this->offset.' Count: '.$this->count);
		
		try {
			$paypal_conf = \Config::get('paypal');
			$apiContext = new ApiContext(new OAuthTokenCredential(
				$paypal_conf['client_id'],
				$paypal_conf['secret'])
			);
			var_dump($paypal_conf);
		    $params = [
				'count' => 10, 
				'start_index' => 0,
				'fields' => 'all',
				'start_date' => '2019-06-12T00:00:00-0700',
				'end_date' => '2014-06-15T23:59:59-0700'
			];
			var_dump($params);
		    $payments = Sync::all($params, $apiContext);
			
			var_dump($payments);
		} catch (Exception $ex) {
			var_dump($ex);
		}
		
		die();
    }
	
	
}
