<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleAttribute;
use App\Magento\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ResetArticleAttributesInMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $shop_id;

    public function __construct($shop_id)
    {
        $this->shop_id = $shop_id;
    }
	
    public function handle()
    {			
		Provider::getInstance($this->shop_id)->authWithMagento();
        
        $result = Provider::getInstance($this->shop_id)->sendRequestToMagento(
            '/rest/V1/products/attributes?searchCriteria[pageSize]=999',
            'GET'
        );
        foreach($result->items as $attribute){
            $result = Provider::getInstance($this->shop_id)->sendRequestToMagento(
                '/rest/V1/products/attributes/'.$attribute->attribute_code,
                'DELETE'
            );
            var_dump('DELETE '.$attribute->attribute_code);
        }
		
		Migration::{'finish_reset_article_attributes_in_magento_shop_'.$this->shop_id}();
	}
	
}