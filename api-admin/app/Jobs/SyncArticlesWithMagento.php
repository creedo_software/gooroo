<?php

namespace App\Jobs;

use App\Migration;
use App\Article;
use App\Magento\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncArticlesWithMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $page;
	protected $limit;
    protected $shop_id;

    public function __construct($shop_id, $page, $limit)
    {
		$this->page = $page;
		$this->limit = $limit;	
        $this->shop_id = $shop_id;
    }

    public function handle()
    {		
        $shop = \App\Shop::where('id', $this->shop_id)->first();
        
	    Provider::getInstance($this->shop_id)->authWithMagento();
	    var_dump('PAGE '.$this->page.' LIMIT '.$this->limit);
		$articles = Provider::getInstance($this->shop_id)->sendRequestToMagento(
			'/rest/all/V1/products?searchCriteria[pageSize]='.$this->limit.'&searchCriteria[currentPage]='.$this->page, 
			'GET'
		);
		
		var_dump('FOUND ', count($articles->items));
		
		foreach($articles->items as $article){
			$exists = \App\Article::where([
				['number', '=', $article->sku],
			])
            ->whereIn('webshop', $shop->getWebshops())
            ->first();
			if(!$exists){
				var_dump('NOT EXISTS '.$article->sku);
                
                $product = [
                    "status" => 0,
                    "sku" => $article->sku,
                ];
                $result = Provider::getInstance($this->shop_id)->sendRequestToMagento(
                    '/rest/'.$shop->store_id.'/V1/products/', 'POST', ['product' => $product]
                );
                var_dump($result);
			}
		}
		
		if(count($articles->items) >= $this->limit){
			$this->page = $this->page + 1;
			SyncArticlesWithMagento::dispatch($this->shop_id, $this->page, $this->limit)
                ->onQueue('sync_articles_with_magento_shop_'.$this->shop_id);
		}else{
			\App\LocalStorage::updateOrCreate(['key' => 'SYNC_ARTICLES_WITH_MAGENTO'], ['value' => 'FINISHED']);
	   }	   
	}
}
