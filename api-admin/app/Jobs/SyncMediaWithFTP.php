<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncMediaWithFTP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $shop_id;

    public function __construct($shop_id)
    {
        $this->shop_id = $shop_id;
    }
	
	public function handle()
    {
        $shop = \App\Shop::where('id', $this->shop_id)->first();        
    	$conn = ftp_connect($shop->settings['ftp_host']); 
        
	    if (ftp_login($conn, $shop->settings['ftp_username'], $shop->settings['ftp_password'])) 
	    { 
			ftp_pasv($conn, true);  
			$contents = ftp_nlist($conn, $shop->settings['ftp_directory']);
            $files_on_ftp = [];
            foreach($contents as $filename){
                preg_match('/^[0]*([1-9][0-9]+)\.[a-z]+$/iU', $filename, $matches);
                if(!$matches || empty($matches[1])){
                    continue;
                }
                $sku = (int) $matches[1];
                $product = \App\Article::where('number', $sku)
                    ->whereIn('webshop', $shop->getWebshops())
                    ->first();
                
                if($product){					
					LoadMediaFromFTP::dispatch(
		                $shop->id,
		                $filename
		            )->onQueue('load_media_from_ftp');
                }
                
                $files_on_ftp[] = $filename;
            }
            
            $all_media = \App\ArticleMedia::where([
                []
            ])
	    }
        
        \App\Job::finish('sync_media_with_ftp_shop_'.$this->shop_id);
    }
}
