<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleAttribute;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadArticleAttributesToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
        $this->shop_id = $shop_id;
    }

    public function handle()
    {		        
        $attributes = ArticleAttribute::where([
                ['shop_id', '=', $this->shop_id],
            ])
            ->get();
            
        foreach($attributes as $attribute){
			$result = $attribute->exportToMagento();
            var_dump($result);
		}
		
		Migration::{'finish_upload_article_attributes_to_magento_shop_'.$this->shop_id}();
	}
}
