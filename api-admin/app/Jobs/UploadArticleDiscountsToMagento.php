<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleDiscount;
use App\Magento\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadArticleDiscountsToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset; 
	protected $count;
    protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = 10;//$count;	
        $this->shop_id = $shop_id;
    }

    public function handle()
    {		
        $shop = \App\Shop::where('id', $this->shop_id)->first();
        
        $kunden = \App\KundeMagento::where([
                ['id_magento', '>', $this->offset],
                ['shop_id', '=', $this->shop_id],
                ['kunde_magento.id_integra', '>', 0],
                ['kunde_integra.id_integra', '>', 0]
            ])
			->join('kunde_integra', 'kunde_integra.id_integra', '=', 'kunde_magento.id_integra')
            ->orderBy('id_magento', 'asc')
            ->paginate($this->count);
        
        foreach($kunden as $kunde){
            var_dump('KUNDE EMAIL: '.$kunde->email);
            $this->offset = $kunde->id_magento;
            //if(!in_array($kunde->email, ['m.mocsnik@gmx.de', 'lakits@t-online.de'])){
            //    continue;
            //}
            $kunde->exportDiscountsToMagento();            
		}
 
		if(count($kunden)){
			Migration::{'next_upload_article_discounts_to_magento_shop_'.$this->shop_id}([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::{'finish_upload_article_discounts_to_magento_shop_'.$this->shop_id}();
	   }
	}
}
