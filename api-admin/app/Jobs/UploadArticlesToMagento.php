<?php

namespace App\Jobs;

use App\Migration;
use App\Article;
use App\Shop;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UploadArticlesToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset; 
	protected $count;
    protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = 5;
        $this->shop_id = $shop_id;       	
    }

    public function handle()
    {
        $shop = Shop::where('id', $this->shop_id)->first();
           
        var_dump('Started with offset '.$this->offset.' and for webshop '.$shop->webshop.'. Count '.$this->count);
           
		$articles = Article::where([
                ['number', '>', $this->offset],
            ])
            ->whereIn('webshop', $shop->getWebshops())
            ->orderBy('number', 'asc')
            ->paginate($this->count);

		foreach($articles as $article){
            var_dump('PROCESSING SKU:', $article->number);
            try{
    			$result = $article->exportToMagento($this->shop_id);
                var_dump($result, $article->number);
            }catch(Exception $e){
                var_dump($e);
            }
			$this->offset = $article->number;
            var_dump('NEW OFFSET IS', $this->offset);
		}
		
		if(count($articles)){
            var_dump('Started new ');
            Migration::{'next_upload_articles_to_magento_shop_'.$this->shop_id}([
                'offset' => $this->offset, 'limit' => $this->count
            ]);
		}else{
			Migration::{'finish_upload_articles_to_magento_shop_'.$this->shop_id}();
            
            \App\Jobs\SyncArticlesWithMagento::dispatch($this->shop_id, 0, 100)
                ->onQueue('sync_articles_with_magento_shop_'.$this->shop_id);
	   }
    }
	
	
}
