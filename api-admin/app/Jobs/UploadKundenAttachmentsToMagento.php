<?php

namespace App\Jobs;

use App\Migration;
use App\KundeAttachment;

use App\Jobs\LoadKundenFromMagento;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UploadKundenAttachmentsToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset; 
	protected $count;
    protected $shop_id;
    
    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = min($count, 10);	
        $this->shop_id = $shop_id;
    }

    public function handle()
    {
	
		$attachments = KundeAttachment::where('id', '>', $this->offset)->whereNull('magento_id')
            ->orderBy('id', 'asc')
            ->paginate($this->count);

		foreach($attachments as $attachment){
			$this->offset = $attachment->id;
            if($attachment->magento_id){
                continue;
            }
            
            $result = $attachment->exportToMagento($this->shop_id);
		}

		if(count($attachments) >= $this->count){
			UploadKundenAttachmentsToMagento::dispatch($this->shop_id, $this->offset, $this->count)
                ->onQueue('upload_kunden_attachments_to_magento_shop_'.$this->shop_id);
		}else{
			Migration::{'finish_upload_kunden_attachments_to_magento_shop_'.$this->shop_id}();
	   }
    }
	
	
}
