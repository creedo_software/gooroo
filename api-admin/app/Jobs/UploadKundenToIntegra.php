<?php

namespace App\Jobs;

use App\Migration;
use App\KundeMagento;
use App\Mapping;

use App\Jobs\JobIntegra;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UploadKundenToIntegra extends JobIntegra
{

	protected $offset; 
	protected $count;
	protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = min($count, 10);	
		$this->shop_id = $shop_id;
    }

    public function process()
    {		
		$kunden = KundeMagento::where('id', '>', $this->offset)
            ->where([
				['email', '=', 'pavelgolman@gmail.com']
			])
            ->orderBy('id', 'asc')
            ->paginate($this->count);

		foreach($kunden as $kunde){
			$this->offset = $kunde->id;
			try{
				$result = $kunde->exportToIntegra();
				var_dump($result);
			}catch(Exception $e){
				var_dump($e);
			}
		}
		
		if(count($kunden) >= $this->count){
			UploadKundenToIntegra::dispatch($this->offset, $this->count)
				->onQueue('upload_kunden_to_integra');
		}else{
			Migration::finish_upload_kunden_to_integra();
	   }
    }
	
	
}
