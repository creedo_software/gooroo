<?php

namespace App\Jobs;

use App\Migration;
use App\KundeIntegra;

use App\Jobs\LoadKundenFromMagento;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UploadKundenToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset; 
	protected $count;
    protected $shop_id;
    
    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = min($count, 10);	
        $this->shop_id = $shop_id;
    }
    
    public function handle()
    {
	
		$kunden = KundeIntegra::where([
                ['id_integra', '>', $this->offset]
            ])
            ->orderBy('id_integra', 'asc')
            ->paginate($this->count);

		foreach($kunden as $kunde){
            var_dump($kunde->email);
			$result = $kunde->exportToMagento($this->shop_id);
			$this->offset = $kunde->id_integra;
		}

		if(count($kunden) >= $this->count){
			UploadKundenToMagento::dispatch($this->shop_id, $this->offset, $this->count)
                ->onQueue('upload_kunden_to_magento_shop_'.$this->shop_id);
		}else{
			Migration::{'finish_upload_kunden_to_magento_shop_'.$this->shop_id}();
            
            //Migration::{'start_load_kunden_from_magento_shop_'.$this->shop_id}();
            //LoadKundenFromMagento::dispatch($this->shop_id, 0, 100)
            //    ->onQueue('load_kunden_from_magento_shop_'.$this->shop_id);
	   }
    }
	
	
}
