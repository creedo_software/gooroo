<?php

namespace App\Jobs;

use App\Article;
use App\ArticleMedia;
use App\Migration;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadMediaToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset; 
	protected $count;
    protected $override;
    protected $shop_id;

    public function __construct($shop_id, $offset, $count, $override = true)
    {
		$this->offset = $offset;
		$this->count = min($count, 10);	
        $this->override = $override;
        $this->shop_id = $shop_id;  
    }

    public function handle()
    {		
        $shop = \App\Shop::where('id', $this->shop_id)->first();
        
		$articles = Article::where([
                ['number', '>', $this->offset],
            ])
            ->whereIn('webshop', $shop->getWebshops())
            ->orderBy('number', 'asc')
            ->paginate($this->count);

		foreach($articles as $article){
			$result = $article->exportMediaToMagento($this->shop_id);
            var_dump($result);
            $this->offset = $article->number;
		}
		
		if(count($articles)){
			UploadMediaToMagento::dispatch($this->shop_id, $this->offset, $this->count, $this->override)
                ->onQueue('upload_media_to_magento_shop_'.$this->shop_id);
		}else{
			Migration::{'finish_upload_media_to_magento_shop_'.$this->shop_id}();
	   }
	}
}
