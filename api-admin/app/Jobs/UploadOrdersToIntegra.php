<?php

namespace App\Jobs;

use App\Migration;
use App\Order;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UploadOrdersToIntegra implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	protected $offset; 
	protected $count;
	protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = min($count, 3);	
		$this->shop_id = $shop_id;
    }

    public function handle()
    {		
		$orders = Order::where([
			['id', '>', $this->offset],
			['shop_id', '=', $this->shop_id]
		])
		->whereNull('integra_beleg_did')
        ->orderBy('id', 'asc')
        ->paginate($this->count);

		foreach($orders as $order){
			$order->uploadToIntegra();
			$this->offset = $order->id;
		}
		
		if(count($orders)){
			UploadOrdersToIntegra::dispatch($this->shop_id, $this->offset, $this->count)
				->onQueue('upload_orders_to_integra_shop_'.$this->shop_id);
		}else{
			Migration::{'finish_upload_orders_to_integra_shop_'.$this->shop_id}();
	   }
    }
	
	
}
