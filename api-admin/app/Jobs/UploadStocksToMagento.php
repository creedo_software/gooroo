<?php

namespace App\Jobs;

use App\Migration;
use App\ArticleStock;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadStocksToMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offset; 
	protected $count;
    protected $shop_id;

    public function __construct($shop_id, $offset, $count)
    {
		$this->offset = $offset;
		$this->count = min($count, 10);	
        $this->shop_id = $shop_id;
    }

    public function handle()
    {		
        $shop = \App\Shop::where('id', $this->shop_id)->first();
        
        $stocks = ArticleStock::where([
                ['sku', '>', $this->offset],
                ['lager', '=', '0001'],
            ])
            ->whereIn('webshop', $shop->getWebshops())
            ->join('articles', 'article_stocks.sku', '=', 'articles.number')
            ->orderBy('sku', 'asc')
            ->paginate($this->count);
            
        foreach($stocks as $stock){
			$result = $stock->exportToMagento($this->shop_id);
            var_dump($stock, $result);
            $this->offset = $stock->sku;
		}
		
		if(count($stocks) >= $this->count){
			UploadStocksToMagento::dispatch($this->shop_id, $this->offset, $this->count)
                ->onQueue('upload_stocks_to_magento_shop_'.$this->shop_id);
		}else{
			Migration::{'finish_upload_stocks_to_magento_shop_'.$this->shop_id}();
	   }
	}
}
