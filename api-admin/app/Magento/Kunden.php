<?php

namespace App\Magento;

use App\Magento\Provider;

class Kunden 
{

	public function upload($kunde){
		$post = [
			  "email" => $kunde->Email,
			  "firstname" => $kunde->Name1,
			  "lastname" => $kunde->Name2,
		];
		
		var_dump($post);
		return Provider::getInstance()->sendRequestToMagento('/rest/V1/customers/', 'POST', ['customer' => $post]);
	
	}

}