<?php

namespace App\Magento;

use App\Magento\Provider;

class Products 
{
	protected $shop_id;
	
	public function __construct($shop_id) {
        $this->shop_id = $shop_id;
	}
	
	public function categories($parentId){
		$categories = Provider::getInstance($this->shop_id)->sendRequestToMagento('/rest/V1/categories/list?searchCriteria[filterGroups][0][filters][0][field]=parent_id&searchCriteria[filterGroups][0][filters][0][value]='.$parentId, 'GET');
		return $categories->items;
	}
	
	public function addNewCategory($parent_id, $categoryName){
		return Provider::getInstance($this->shop_id)->sendRequestToMagento('/rest/V1/categories', 'POST', ["category" => [
		    "name" => $categoryName,
		    "parent_id" => $parent_id,
			"is_active" => true,
		]]);
	}
	
	public function getMedia($sku){
		return Provider::getInstance($this->shop_id)->sendRequestToMagento('/rest/V1/products/'.$sku.'/media', 'GET');
	}
	
	public function attributesList($attributeCode){
       return Provider::getInstance($this->shop_id)->sendRequestToMagento('/rest/V1/products/attributes/'.$attributeCode.'/options/', 'GET');
   }
   
   public function addNewAttributeValue($attributeCode, $value){
       return Provider::getInstance($this->shop_id)->sendRequestToMagento('/rest/V1/products/attributes/'.$attributeCode.'/options/', 'POST', ["option" => [
           "label" => $value,
           "value" => $value,
       ]]);
   }
	
}