<?php

namespace App\Magento;

use App\Shop;

class Provider 
{
    private static $instances = [];
	
    protected $shop;
	protected $AuthToken;
	
    /**
     * @return Singleton
     */
    public static function getInstance($shop_id)
    {
        
        if (empty(self::$instances[$shop_id]))
        {
            self::$instances[$shop_id] = new self($shop_id);
        }
        return self::$instances[$shop_id];
    }
	
    private function __clone() {}
    private function __construct($shop_id) {
        $this->shop = Shop::where('id', $shop_id)->first();
		$this->authWithMagento();
	}

	public function authWithMagento()
    {
        $userData = array("username" => $this->shop->username, "password" => $this->shop->password);
		$this->AuthToken = $this->sendRequestToMagento(
			"/rest/V1/integration/admin/token", 
			"POST", 
			$userData, 
			array("Content-Type: application/json; charset=utf-8”, “Content-Lenght: " . strlen(json_encode($userData))),
            false
		);
		//var_dump('RECEIVED AUTH KEY: ', $this->AuthToken);
	}
	
	public function sendRequestToMagento($url, $method, $params = null, $headers = null, $need_decode = true, $need_encode_params = true, $content_type='application/json'){
        //var_dump($url);
		$ch = curl_init($this->shop->host.$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		if($params){
			curl_setopt($ch, CURLOPT_POSTFIELDS, $need_encode_params ? json_encode($params) : $params);
		}
		if(empty($headers)){
			$headers = [
				"Accept: application/json",
				"Content-Type: ".$content_type, 
				"Authorization: Bearer " . json_decode($this->AuthToken)
			];
		}

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //curl_setopt($ch, CURLOPT_PROXY, $proxy);     // PROXY details with port
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);   // Use if proxy have username and password
        //curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5); 

        $result = curl_exec($ch);

        if($need_decode){
            $decoded = json_decode($result);
            if(empty($decoded)){
                //var_dump('UNDECODED RESULT:', $result);
            }
            return $decoded;
        }

        return $result;
    }   
}
