<?php

namespace App\Soap\Response;

class AuthToken
{

  /**
   * @var string
   */
  protected $SessionID;

  /**
   * ConnectionInfo constructor.
   *
   * @param string $SessionID
   */
  public function __construct($SessionID)
  {
    $this->SessionID = $SessionID;
  }

  /**
   * @return string
   */
  public function getSessionID()
  {
    return $this->SessionID;
  }

}