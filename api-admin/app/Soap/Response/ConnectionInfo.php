<?php

namespace App\Soap\Response;

use AuthToken;

class ConnectionInfo
{

  /**
   * @var string
   */
  protected $Host;

  /**
   * @var int
   */
  protected $Port;

  /**
   * @var AuthToken
   */
  protected $AuthToken;

  /**
   * @var string
   */
  protected $DebugInfo;

  /**
   * ConnectionInfo constructor.
   *
   * @param string $Host
   * @param string $Port
   * @param string $AuthToken
   * @param string $DebugInfo
   */
  public function __construct($Host, $Port, $AuthToken, $DebugInfo)
  {
    $this->Host			= $Host;
    $this->Port			= $Port;
	$this->AuthToken	= $AuthToken;
    $this->DebugInfo	= $DebugInfo;
  }

  /**
   * @return string
   */
  public function getHost()
  {
    return $this->Host;
  }

  /**
   * @return int
   */
  public function getPort()
  {
    return $this->Port;
  }

  /**
   * @return AuthToken
   */
  public function getAuthToken()
  {
    return $this->AuthToken;
  }

  /**
   * @return string
   */
  public function getDebugInfo()
  {
    return $this->DebugInfo;
  }
}