<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKundeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kunde', function (Blueprint $table) {    
            $table->increments('id');
                                
            $table->string('email', 200)->unique();
			$table->json('attributes_integra')->nullable();
            $table->json('attributes_magento')->nullable();
            $table->integer('id_integra')->nullable();
            $table->integer('id_magento')->nullable();
            
            $table->index('id_integra');
            $table->index('id_magento');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kunde');
    }
}
