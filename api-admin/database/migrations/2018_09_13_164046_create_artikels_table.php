<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtikelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 100)->nullable()->unique();
            $table->json('attributes')->nullable();
            $table->json('additional_attributes')->nullable();
            $table->string('webshop', 100)->nullable();
            $table->unsignedInteger('magento_id')->nullable();
            
            $table->index('webshop');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
