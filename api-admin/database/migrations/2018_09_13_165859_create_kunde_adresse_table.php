<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKundeAdresseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
 	{
 		Schema::create('kunde_adresse', function (Blueprint $table) {
 			$table->unsignedInteger('KundeNumber');
 			$table->string('Strabe');
            $table->string('PLZ');
            $table->string('Stadt');
 			
 			$table->primary('KundeNumber');
 		});
 	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kunde_adresse');
    }
}
