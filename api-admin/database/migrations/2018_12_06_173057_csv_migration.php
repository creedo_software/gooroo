<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CsvMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_imported', function (Blueprint $table) {
            $table->string('sku_shop_id', 100);
            
            $table->string('sku', 100);
            $table->unsignedInteger('shop_id');
            $table->unsignedInteger('attribute_set_id');
            $table->unsignedInteger('visibility');
            $table->unsignedInteger('quantity');
            $table->string('name', 200);
            $table->float('price', 8, 2);
            $table->float('weight', 8, 2);
            $table->json('custom_attributes');
            $table->timestamps();
    
            $table->primary('sku_shop_id');
            $table->index('shop_id');
            $table->index('sku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csv_imported');
    }
}
