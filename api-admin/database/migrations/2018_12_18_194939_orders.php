<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id');
            $table->string('email')->nullable();

            $table->unsignedInteger('magento_order_id');//entity_id
            $table->json('magento_attributes');
            
            $table->string('integra_beleg_did')->nullable();
            $table->longText('integra_order')->nullable();
            $table->json('integra_items');
            $table->timestamps();
    
            $table->unique('integra_beleg_did');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
