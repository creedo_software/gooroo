<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_media', function (Blueprint $table) {    
            $table->increments('id');
            $table->integer('sku');	
            $table->string('mimetype', 100);  
            $table->string('name', 200);    
            $table->string('storage_id', 200);     
            $table->string('path', 200);          
            $table->string('label', 200);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_media');
    }
}
