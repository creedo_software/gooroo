<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Mapping;

class MappingArticles1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Mapping::add(1, "magento", "article", "name", ["type" => "mapped", "value" => "BEZEICHNUNG"]);
		Mapping::add(1, "magento", "article", "weight", ["type" => "fixed", "value" => ""]);
        
        Mapping::add(2, "magento", "article", "name", ["type" => "mapped", "value" => "BEZEICHNUNG"]);
		Mapping::add(2, "magento", "article", "weight", ["type" => "fixed", "value" => ""]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}

