<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Mapping;

class MappingKunden extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Mapping::add(0, "magento", "kunden", "group_id", ["type" => "fixed", "value" => "1"]);
		Mapping::add(0, "magento", "kunden", "dob", ["type" => "fixed", "value" => ""]);
		Mapping::add(0, "magento", "kunden", "email", ["type" => "mapped", "value" => "EMAIL"]);
		Mapping::add(0, "magento", "kunden", "firstname", ["type" => "mapped", "value" => "NAME1"]);
		Mapping::add(0, "magento", "kunden", "lastname", ["type" => "mapped", "value" => "NAME2"]);
		Mapping::add(0, "magento", "kunden", "middlename", ["type" => "mapped", "value" => "ANREDE"]);
		Mapping::add(0, "magento", "kunden", "prefix", ["type" => "fixed", "value" => ""]);
		Mapping::add(0, "magento", "kunden", "suffix", ["type" => "fixed", "value" => ""]);
		Mapping::add(0, "magento", "kunden", "gender", ["type" => "fixed", "value" => ""]);
		Mapping::add(0, "magento", "kunden", "store_id", ["type" => "fixed", "value" => "0"]);
		Mapping::add(0, "magento", "kunden", "taxvat", ["type" => "fixed", "value" => ""]);
        
        
        Mapping::add(0, "integra", "kunden", "Name1", ["type" => "mapped", "value" => "firstname"]);
        Mapping::add(0, "integra", "kunden", "Name2", ["type" => "mapped", "value" => "lastname"]);
        Mapping::add(0, "integra", "kunden", "Name3", ["type" => "mapped", "value" => "middlename"]);
        Mapping::add(0, "integra", "kunden", "Mail", ["type" => "mapped", "value" => "email"]);
        Mapping::add(0, "integra", "kunden", "Whrg", ["type" => "fixed", "value" => "EUR"]);
        Mapping::add(0, "integra", "kunden", "Sprache", ["type" => "fixed", "value" => "1"]);
        Mapping::add(0, "integra", "kunden", "UstIdNr", ["type" => "fixed", "value" => ""]);
        Mapping::add(0, "integra", "kunden", "KdGrp", ["type" => "fixed", "value" => "01"]);
        Mapping::add(0, "integra", "kunden", "Status", ["type" => "fixed", "value" => "2"]);
        Mapping::add(0, "integra", "kunden", "KZTeilLief", ["type" => "fixed", "value" => "1"]);
        Mapping::add(0, "integra", "kunden", "NrKdArt", ["type" => "fixed", "value" => "2"]);
        Mapping::add(0, "integra", "kunden", "PreisGrp", ["type" => "fixed", "value" => "03"]);
        Mapping::add(0, "integra", "kunden", "SteuerSchl", ["type" => "fixed", "value" => "1"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
