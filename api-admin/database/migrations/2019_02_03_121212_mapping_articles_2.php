<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Mapping;

class MappingArticles2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Mapping::add(1, "magento", "article", "description", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "country_of_manufacture", ["type" => "fixed", "value" => "IT"]);
		Mapping::add(1, "magento", "article", "short_description", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "meta_title", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "meta_description", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "product_seo_name", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "url_key", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "ausbau", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "boden", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "link", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "weinart", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "ean_fl", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "anbauregion", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "schaumweinkategorie", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "gebinde", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "fasche", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "jahrgang", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "anbaugebiet", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "erziehungsform", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "gewicht", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "rebsorte", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "flascheninhalt", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "lagerfahigkeit", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "vk_brutto", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "jahresproduktion", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "alkohol", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "bestand", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "ertrag_hl_h", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "gesamtsaure", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "serviertemperatur", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "restzucker", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "alter_der_reben", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "geschmacksrichtung", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "schaumweinart", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "weinstilistik", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "verschlussart", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "enthalt_gelatine", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "enthalt_huhnereiweiss", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "enthalt_milcheiweiss", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "enthalt_sulfite", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "pradikat", ["type" => "mapped", "value" => ""]);
		Mapping::add(1, "magento", "article", "prasentartikel", ["type" => "mapped", "value" => ""]);
        
        
        
        Mapping::add(2, "magento", "article", "description", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "country_of_manufacture", ["type" => "fixed", "value" => "IT"]);
		Mapping::add(2, "magento", "article", "short_description", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "meta_title", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "meta_description", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "product_seo_name", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "url_key", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "ausbau", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "boden", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "link", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "weinart", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "ean_fl", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "anbauregion", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "schaumweinkategorie", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "gebinde", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "fasche", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "jahrgang", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "anbaugebiet", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "erziehungsform", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "gewicht", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "rebsorte", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "flascheninhalt", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "lagerfahigkeit", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "vk_brutto", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "jahresproduktion", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "alkohol", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "bestand", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "ertrag_hl_h", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "gesamtsaure", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "serviertemperatur", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "restzucker", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "alter_der_reben", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "geschmacksrichtung", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "schaumweinart", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "weinstilistik", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "verschlussart", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "enthalt_gelatine", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "enthalt_huhnereiweiss", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "enthalt_milcheiweiss", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "enthalt_sulfite", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "pradikat", ["type" => "mapped", "value" => ""]);
		Mapping::add(2, "magento", "article", "prasentartikel", ["type" => "mapped", "value" => ""]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}

