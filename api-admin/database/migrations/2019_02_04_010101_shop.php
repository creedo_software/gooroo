<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Shop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
			$table->string('host');
			$table->string('username')->nullable();
			$table->string('password')->nullable();
            $table->string('pg')->nullable();
            $table->string('store_id')->nullable();
            $table->integer('attribute_set_id')->nullable();
            $table->integer('category_all_id')->nullable();
            $table->integer('category_herstellers_id')->nullable();
            $table->integer('category_region_id')->nullable();
            $table->string('webshop')->nullable();
            
            $table->timestamps();
    
            $table->unique('host');
            $table->unique('pg');
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
