<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticlePrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_prices', function (Blueprint $table) {
            $table->string('nummer_pg');
            $table->integer('nummer');
            $table->string('bs')->nullable();
			$table->string('prvar')->nullable();
			$table->string('pg')->nullable();
			$table->decimal('netto_brutto', 8, 2)->nullable();
			$table->decimal('price', 8, 2)->nullable();
            $table->decimal('price_ak', 8, 2)->nullable();
			$table->string('satzstatus')->nullable();
            $table->timestamps();
    
			$table->primary('nummer_pg');
            $table->index('nummer');
            $table->index('pg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_prices');
    }
}
