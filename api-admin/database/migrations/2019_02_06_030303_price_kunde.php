<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pricekunde extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_kunden', function (Blueprint $table) {
            $table->string('kd_art_nr');
            
            $table->integer('kd_nr');
			$table->integer('art_nr');
			
            $table->string('bs')->nullable();
			$table->string('prvar')->nullable();
			$table->string('spkms')->nullable();
			$table->integer('rabattfahig')->nullable();
			$table->decimal('preis2', 8, 2)->nullable();
			$table->string('satzstatus')->nullable();
            $table->timestamps();
    
			$table->primary('kd_art_nr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_kunden');
    }
}
