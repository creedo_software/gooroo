<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticleDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_discounts', function (Blueprint $table) {
            $table->string('zaidnr_zaabme');
            $table->string('zabtnr')->nullable();
            $table->integer('zaidnr');
			$table->string('zaabta')->nullable();
			$table->string('zatyp')->nullable();
			$table->string('zarbasis')->nullable();
			$table->string('zainex')->nullable();
			$table->decimal('zarang', 8, 2)->nullable();
			$table->decimal('zaabme', 8, 2)->nullable();
			$table->string('zaartn')->nullable();
			$table->string('zavar')->nullable();
			$table->string('zawg')->nullable();
			$table->string('zaerzc')->nullable();
			$table->string('zawa1')->nullable();
			$table->string('zawa2')->nullable();
			$table->string('zawa3')->nullable();
			$table->string('zawa4')->nullable();
			$table->string('zasort')->nullable();
			$table->string('zageba')->nullable();
			$table->string('zagebi')->nullable();
			$table->string('zaabfa')->nullable();
			$table->string('zawert_neu')->nullable();
			$table->string('zakzab')->nullable();
            $table->timestamps();
    
			$table->primary('zaidnr_zaabme');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_discounts');
    }
}
