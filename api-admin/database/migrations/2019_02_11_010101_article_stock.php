<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticleStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_stocks', function (Blueprint $table) {
            $table->integer('sku');
            $table->string('lager');
             $table->string('sku_lager');
			$table->decimal('quantity', 8, 2);
            $table->json('details')->nullable();
            $table->timestamps();
    
			$table->primary('sku_lager');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_stocks');
    }
}
