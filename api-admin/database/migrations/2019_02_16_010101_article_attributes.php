<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\ArticleAttribute;

class ArticleAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_attributes', function (Blueprint $table) {
			$table->string('shop_attribute_code');
			
            $table->integer('shop_id');
			$table->integer('attribute_group_id')->nullable();
			$table->string('attribute_code');
			$table->string('attribute_name');
            $table->string('frontend_input');
			$table->string('default_value');
			$table->string('options');
			$table->json('magento')->nullable();
            $table->timestamps();
    
			$table->primary('shop_attribute_code');
            
            $table->index('attribute_code');
            $table->index('shop_id');
            $table->index('attribute_group_id');
        });
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_geschmacksrichtung', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'geschmacksrichtung', 'attribute_name' => 'Geschmacksrichtung', 
	      'frontend_input' => 'select', 'default_value' => 'trocken',	
	      'options' => 'trocken,halbtrocken,lieblich,süß',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_ausbau', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'ausbau', 'attribute_name' => 'Ausbau',
	      'frontend_input' => 'select', 'default_value' => 'barriquefass',	
	      'options' => 'amphoren,barriquefass,betontank,holzfass,holz/barriquefass,holzfass/edelstahltank,edelstahltank,edelstahl/barriquefass,edelstahltank/betontank',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_weinstilistik', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'weinstilistik', 'attribute_name' => 'Weinstilistik', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ausgewogen,leicht,prickelnd,süß,vollmundig,fruchtbetont,vollmundig & komplex,ausgewogen & elegant,leicht & frisch,prickelnd & frisch,süß & reif',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_essensbegleitung', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'essensbegleitung', 'attribute_name' => 'Essensbegleitung', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'fisch,dessert,vorspeise,salat,wild,fleisch dunkel,fleisch hell,geflügel dunkel,geflügel hell,vegetarisch,meeresfrüchte,käse reif,käse jung,pizza und pasta',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_verschlussart', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'verschlussart', 'attribute_name' => 'Verschlussart', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'naturkorken,diam korken,presskorken,scheibenkork,2 scheibenkork,kunststoffkorken,schraubverschluss,glaskorken (vinolok),sektkorken,spago',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_schaumweinkategorie', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'schaumweinkategorie', 'attribute_name' => 'Schaumweinkategorie', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'brut,extra dry,dry,demi-sec,extra brut',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_schaumweinart', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'schaumweinart', 'attribute_name' => 'Schaumweinart', 
	      'frontend_input' => 'select', 'default_value' => 'trocken',	
	      'options' => 'spumante,frizzante',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_weinart', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'weinart', 'attribute_name' => 'Weinart', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'weißwein,rotwein,roséwein,schaumwein,dessertwein,spirituosen',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_jahrgang', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'jahrgang', 'attribute_name' => 'Jahrgang', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => '--2018--,--2017--,--2016--,--2015--,--2014--,--2013--,--2012--,--2011--,--2010--,--2009--,--2008--,--2007--,--2006--',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_flascheninhalt', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'flascheninhalt', 'attribute_name' => 'Flascheninhalt', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => '6,5,4.2,3,1.5,1,0.75,0.7,0.5,0.4,0.375,0.3,0.2,0.1',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_anbauregion', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'anbauregion', 'attribute_name' => 'Anbauregion', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'trentino-südtirol,friaul-julisch venetien,toskana,piemont,venetien,sardinien,lombardei,sizilien,kampanien,umbrien,abruzzen,emilia-romagna,marken,apulien,aostatal',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_pradikat', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'pradikat', 'attribute_name' => 'Pradikat', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'doc,docg,igt,igp,dop,v.s.q.',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_enthalt_gelatine', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_gelatine', 'attribute_name' => 'Enthalt Gelatine', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_enthalt_huhnereiweiss', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_huhnereiweiss', 'attribute_name' => 'Enthalt Huhnereiweiss', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_enthalt_milcheiweiss', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_milcheiweiss', 'attribute_name' => 'Enthalt Milcheiweiss', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_enthalt_sulfite', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_sulfite', 'attribute_name' => 'Enthalt Sulfite', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '1_prasentartikel', 'shop_id' => 1, 'backend_type' => 'int',
	      'attribute_code' => 'prasentartikel', 'attribute_name' => 'Prasentartikel', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		////////////////////////////////////////////////////
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_geschmacksrichtung', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'geschmacksrichtung', 'attribute_name' => 'Geschmacksrichtung', 
	      'frontend_input' => 'select', 'default_value' => 'trocken',	
	      'options' => 'trocken,halbtrocken,lieblich,süß',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_ausbau', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'ausbau', 'attribute_name' => 'Ausbau',
	      'frontend_input' => 'select', 'default_value' => 'barriquefass',	
	      'options' => 'amphoren,barriquefass,betontank,holzfass,holz/barriquefass,holzfass/edelstahltank,edelstahltank,edelstahl/barriquefass,edelstahltank/betontank',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_weinstilistik', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'weinstilistik', 'attribute_name' => 'Weinstilistik', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ausgewogen,leicht,prickelnd,süß,vollmundig,fruchtbetont,vollmundig & komplex,ausgewogen & elegant,leicht & frisch,prickelnd & frisch,süß & reif',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_essensbegleitung', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'essensbegleitung', 'attribute_name' => 'Essensbegleitung', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'fisch,dessert,vorspeise,salat,wild,fleisch dunkel,fleisch hell,geflügel dunkel,geflügel hell,vegetarisch,meeresfrüchte,käse reif,käse jung,pizza und pasta',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_verschlussart', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'verschlussart', 'attribute_name' => 'Verschlussart', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'naturkorken,diam korken,presskorken,scheibenkork,2 scheibenkork,kunststoffkorken,schraubverschluss,glaskorken (vinolok),sektkorken,spago',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_schaumweinkategorie', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'schaumweinkategorie', 'attribute_name' => 'Schaumweinkategorie', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'brut,extra dry,dry,demi-sec,extra brut',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_schaumweinart', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'schaumweinart', 'attribute_name' => 'Schaumweinart', 
	      'frontend_input' => 'select', 'default_value' => 'trocken',	
	      'options' => 'spumante,frizzante',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_weinart', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'weinart', 'attribute_name' => 'Weinart', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'weißwein,rotwein,roséwein,schaumwein,dessertwein,spirituosen',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_jahrgang', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'jahrgang', 'attribute_name' => 'Jahrgang', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => '--2018--,--2017--,--2016--,--2015--,--2014--,--2013--,--2012--,--2011--,--2010--,--2009--,--2008--,--2007--,--2006--',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_flascheninhalt', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'flascheninhalt', 'attribute_name' => 'Flascheninhalt', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => '6,5,4.2,3,1.5,1,0.75,0.7,0.5,0.4,0.375,0.3,0.2,0.1',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_anbauregion', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'anbauregion', 'attribute_name' => 'Anbauregion', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'trentino-südtirol,friaul-julisch venetien,toskana,piemont,venetien,sardinien,lombardei,sizilien,kampanien,umbrien,abruzzen,emilia-romagna,marken,apulien,aostatal',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_pradikat', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'pradikat', 'attribute_name' => 'Pradikat', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'doc,docg,igt,igp,dop,v.s.q.',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_enthalt_gelatine', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_gelatine', 'attribute_name' => 'Enthalt Gelatine', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_enthalt_huhnereiweiss', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_huhnereiweiss', 'attribute_name' => 'Enthalt Huhnereiweiss', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_enthalt_milcheiweiss', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_milcheiweiss', 'attribute_name' => 'Enthalt Milcheiweiss', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_enthalt_sulfite', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'enthalt_sulfite', 'attribute_name' => 'Enthalt Sulfite', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
		
		ArticleAttribute::create([
	      'shop_attribute_code' => '2_prasentartikel', 'shop_id' => 2, 'backend_type' => 'int',
	      'attribute_code' => 'prasentartikel', 'attribute_name' => 'Prasentartikel', 
	      'frontend_input' => 'select', 'default_value' => '',	
	      'options' => 'ja,nein',	
	    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_attributes');
    }
}
