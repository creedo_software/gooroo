<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\ArticleAttribute;

class ArticleAttributesText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
		foreach([1, 2] as $shop_id){
			foreach(['boden', 'link', 'ean_fl', 'gebinde', 'fasche', 'anbaugebiet', 
			'erziehungsform', 'gewicht', 'rebsorte', 'lagerfahigkeit', 
			'jahresproduktion', 'alkohol', 'bestand', 'ertrag_hl_h', 'vk_brutto',
			'gesamtsaure', 'serviertemperatur', 'restzucker', 'alter_der_reben'] as $key_name){
				ArticleAttribute::create([
			      'shop_attribute_code' => $shop_id.'_'.$key_name, 'shop_id' => $shop_id, 'backend_type' => 'varchar',
			      'attribute_code' => $key_name, 'attribute_name' => ucfirst($key_name), 
			      'frontend_input' => 'text', 'default_value' => '', 'options' => '',	
			    ]);
			}
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
