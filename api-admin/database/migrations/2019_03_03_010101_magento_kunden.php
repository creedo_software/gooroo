<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MagentoKunden extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kunde_magento', function (Blueprint $table) {
            $table->string('shop_id_email');
            $table->string('email', 200)->unique();
            $table->json('kunde_attributes')->nullable();
            $table->integer('id_magento')->nullable();
			$table->integer('shop_id')->nullable();
            
			$table->primary('shop_id_email');
            $table->index('id_magento');
			$table->index('email');
			$table->index('shop_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kunde_magento');
    }
}
