<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IntegraKunden extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kunde_integra', function (Blueprint $table) {
            $table->string('email', 200)->unique();
            $table->json('kunde_attributes')->nullable();
            $table->integer('id_integra')->nullable();
            
			$table->primary('email');
            $table->index('id_integra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kunde_integra');
    }
}
