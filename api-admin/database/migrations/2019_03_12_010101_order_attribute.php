<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_attributes', function (Blueprint $table) {
			$table->string('shop_attribute_code');
			
            $table->integer('shop_id');
			$table->string('attribute_code');
			$table->string('attribute_value')->nullable();
            
            $table->timestamps();
    
			$table->primary('shop_attribute_code');
            
            $table->index('attribute_code');
            $table->index('shop_id');
            $table->index('attribute_value');
        });
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Vorgangsart', 'shop_id' => 2, 
	      'attribute_code' => 'Vorgangsart', 'attribute_value' => '1015',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Belegart', 'shop_id' => 2, 
	      'attribute_code' => 'Belegart', 'attribute_value' => '40',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_NetBrut', 'shop_id' => 2, 
	      'attribute_code' => 'NetBrut', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_NrKdVersand', 'shop_id' => 2, 
	      'attribute_code' => 'NrKdVersand', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Versandart', 'shop_id' => 2, 
	      'attribute_code' => 'Versandart', 'attribute_value' => 'DHL',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Zahlungskondition', 'shop_id' => 2, 
	      'attribute_code' => 'Zahlungskondition', 'attribute_value' => '01',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Zahlungsart', 'shop_id' => 2, 
	      'attribute_code' => 'Zahlungsart', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_KzGutschrift', 'shop_id' => 2, 
	      'attribute_code' => 'KzGutschrift', 'attribute_value' => '0',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Stichwort', 'shop_id' => 2, 
	      'attribute_code' => 'Stichwort', 'attribute_value' => 'Test ohne Calculate',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_NotizIntern', 'shop_id' => 2, 
	      'attribute_code' => 'NotizIntern', 'attribute_value' => 'test',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_KopfText', 'shop_id' => 2, 
	      'attribute_code' => 'KopfText', 'attribute_value' => 'test',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_FussText', 'shop_id' => 2, 
	      'attribute_code' => 'FussText', 'attribute_value' => 'test',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Gesperrt', 'shop_id' => 2, 
	      'attribute_code' => 'Gesperrt', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Mitarbeiter', 'shop_id' => 2, 
	      'attribute_code' => 'Mitarbeiter', 'attribute_value' => '9997',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Mengenbezeichnung', 'shop_id' => 2, 
	      'attribute_code' => 'Mengenbezeichnung', 'attribute_value' => 'FL',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '2_Steuerschluessel', 'shop_id' => 2, 
	      'attribute_code' => 'Steuerschluessel', 'attribute_value' => '1',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '2_PositionArt', 'shop_id' => 2, 
	      'attribute_code' => 'PositionArt', 'attribute_value' => '10',	
	    ]);
        
        /////////////////////////////////////////
        
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Vorgangsart', 'shop_id' => 3, 
	      'attribute_code' => 'Vorgangsart', 'attribute_value' => '1015',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Belegart', 'shop_id' => 3, 
	      'attribute_code' => 'Belegart', 'attribute_value' => '40',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_NetBrut', 'shop_id' => 3, 
	      'attribute_code' => 'NetBrut', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_NrKdVersand', 'shop_id' => 2, 
	      'attribute_code' => 'NrKdVersand', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Versandart', 'shop_id' => 3, 
	      'attribute_code' => 'Versandart', 'attribute_value' => 'DHL',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Zahlungskondition', 'shop_id' => 3, 
	      'attribute_code' => 'Zahlungskondition', 'attribute_value' => '01',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Zahlungsart', 'shop_id' => 3, 
	      'attribute_code' => 'Zahlungsart', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_KzGutschrift', 'shop_id' => 3, 
	      'attribute_code' => 'KzGutschrift', 'attribute_value' => '0',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Stichwort', 'shop_id' => 3, 
	      'attribute_code' => 'Stichwort', 'attribute_value' => 'Test ohne Calculate',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_NotizIntern', 'shop_id' => 3, 
	      'attribute_code' => 'NotizIntern', 'attribute_value' => 'test',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_KopfText', 'shop_id' => 3, 
	      'attribute_code' => 'KopfText', 'attribute_value' => 'test',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_FussText', 'shop_id' => 3, 
	      'attribute_code' => 'FussText', 'attribute_value' => 'test',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Gesperrt', 'shop_id' => 3, 
	      'attribute_code' => 'Gesperrt', 'attribute_value' => '1',	
	    ]);
		
		\App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Mitarbeiter', 'shop_id' => 3, 
	      'attribute_code' => 'Mitarbeiter', 'attribute_value' => '9997',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Mengenbezeichnung', 'shop_id' => 3, 
	      'attribute_code' => 'Mengenbezeichnung', 'attribute_value' => 'FL',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '3_Steuerschluessel', 'shop_id' => 3, 
	      'attribute_code' => 'Steuerschluessel', 'attribute_value' => '1',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '3_PositionArt', 'shop_id' => 3, 
	      'attribute_code' => 'PositionArt', 'attribute_value' => '10',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '3_DHL_offset', 'shop_id' => 3, 
	      'attribute_code' => 'DHL_offset', 'attribute_value' => '2',	
	    ]);
        
        \App\OrderAttribute::create([
	      'shop_attribute_code' => '2_DHL_offset', 'shop_id' => 2, 
	      'attribute_code' => 'DHL_offset', 'attribute_value' => '2',	
	    ]);
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_attributes');
    }
}
