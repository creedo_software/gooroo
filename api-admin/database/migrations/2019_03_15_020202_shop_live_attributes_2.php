<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\ArticleAttribute;

class ShopLiveAttributes2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			foreach(['boden', 'link', 'ean_fl', 'gebinde', 'fasche', 'anbaugebiet', 
			'erziehungsform', 'gewicht', 'rebsorte', 'lagerfahigkeit', 
			'jahresproduktion', 'alkohol', 'bestand', 'ertrag_hl_h', 'vk_brutto',
			'gesamtsaure', 'serviertemperatur', 'restzucker', 'alter_der_reben'] as $key_name){
				ArticleAttribute::create([
			      'shop_attribute_code' => '3_'.$key_name, 'shop_id' => 3, 'backend_type' => 'varchar',
			      'attribute_code' => $key_name, 'attribute_name' => ucfirst($key_name), 
			      'frontend_input' => 'text', 'default_value' => '', 'options' => '',	
			    ]);
			}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
