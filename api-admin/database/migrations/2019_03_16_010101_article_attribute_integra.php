<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticleAttributeIntegra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_attribute_integra', function (Blueprint $table) {
			$table->string('attribute_code');
			$table->json('attribute_values')->nullable();
            
            $table->timestamps();
    
			$table->primary('attribute_code');
        });
		
		\App\ArticleAttributeIntegra::create([
            'attribute_code' => 'ausbau', 
            'attribute_values' => json_encode([
                'amphoren' => '1',
                'barriquefass' => '2',
                'betontank' => '6',
                'holzfass' => '7',
                'holz/barriquefass' => '8',
                'holzfass/edelstahltank' => '10',
                'edelstahltank' => '11',
                'edelstahl/barriquefass' => '12',
                'edelstahltank/betontank' => '14',
            ]),	
	    ]);
		
		\App\ArticleAttributeIntegra::create([
            'attribute_code' => 'geschmacksrichtung', 
            'attribute_values' => json_encode([
                'trocken' => '',
                'halbtrocken' => '',
                'lieblich' => '',
                'süß' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'weinstilistik', 
            'attribute_values' => json_encode([
                'ausgewogen' => '',
                'leicht' => '',
                'prickelnd' => '',
                'süß' => '',
                'vollmundig' => '',
                'fruchtbetont' => '',
                'vollmundig & komplex' => '',
                'ausgewogen & elegant' => '',
                'leicht & frisch' => '',
                'prickelnd & frisch' => '',
                'süß & reif' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'essensbegleitung', 
            'attribute_values' => json_encode([
                'fisch' => '',
                'dessert' => '',
                'vorspeise' => '',
                'salat' => '',
                'wild' => '',
                'fleisch dunkel' => '',
                'fleisch hell' => '',
                'geflügel dunkel' => '',
                'geflügel hell' => '',
                'vegetarisch' => '',
                'meeresfrüchte' => '',
                'käse reif' => '',
                'käse jung' => '',
                'pizza und pasta' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'verschlussart', 
            'attribute_values' => json_encode([
                'naturkorken' => '',
                'diam korken' => '',
                'presskorken' => '',
                'scheibenkork' => '',
                '2 scheibenkork' => '',
                'kunststoffkorken' => '',
                'schraubverschluss' => '',
                'glaskorken (vinolok)' => '',
                'sektkorken,spago' => '',
            ]),	
	    ]);

        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'schaumweinkategorie', 
            'attribute_values' => json_encode([
                'brut' => '',
                'extra dry' => '',
                'dry' => '',
                'demi-sec' => '',
                'extra brut' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'schaumweinart', 
            'attribute_values' => json_encode([
                'spumante' => '',
                'frizzante' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'weinart', 
            'attribute_values' => json_encode([
                'weißwein' => '',
                'rotwein' => '',
                'roséwein' => '',
                'schaumwein' => '',
                'dessertwein' => '',
                'spirituosen' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'jahrgang', 
            'attribute_values' => json_encode([
                '--2018--' => '',
                '--2017--' => '',
                '--2016--' => '',
                '--2015--' => '',
                '--2014--' => '',
                '--2013--' => '',
                '--2012--' => '',
                '--2011--' => '',
                '--2010--' => '',
                '--2009--' => '',
                '--2008--' => '',
                '--2007--' => '',
                '--2006--' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'flascheninhalt', 
            'attribute_values' => json_encode([
                '6' => '',
                '5' => '',
                '4.2' => '',
                '3' => '',
                '1.5' => '',
                '1' => '',
                '0.75' => '',
                '0.7' => '',
                '0.5' => '',
                '0.4' => '',
                '0.375' => '',
                '0.3' => '',
                '0.2' => '',
                '0.1' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'anbauregion', 
            'attribute_values' => json_encode([
                'trentino-südtirol' => '',
                'friaul-julisch venetien' => '',
                'toskana' => '',
                'piemont' => '',
                'venetien' => '',
                'sardinien' => '',
                'lombardei' => '',
                'sizilien' => '',
                'kampanien' => '',
                'umbrien' => '',
                'abruzzen' => '',
                'emilia-romagna' => '',
                'marken' => '',
                'apulien' => '',
                'aostatal' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'pradikat', 
            'attribute_values' => json_encode([
                'doc' => '',
                'docg' => '',
                'igt' => '',
                'igp' => '',
                'dop' => '',
                'v.s.q.' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'enthalt_gelatine', 
            'attribute_values' => json_encode([
                'ja' => '',
                'nein' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'enthalt_huhnereiweiss', 
            'attribute_values' => json_encode([
                'ja' => '',
                'nein' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'enthalt_milcheiweiss', 
            'attribute_values' => json_encode([
                'ja' => '',
                'nein' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'enthalt_sulfite', 
            'attribute_values' => json_encode([
                'ja' => '',
                'nein' => '',
            ]),	
	    ]);
        
        \App\ArticleAttributeIntegra::create([
            'attribute_code' => 'prasentartikel', 
            'attribute_values' => json_encode([
                'ja' => '',
                'nein' => '',
            ]),	
	    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_attribute_integra');
    }
}
