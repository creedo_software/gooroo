<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KundenAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kunden_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('integra_kunde_id');
            $table->float('total');
			$table->string('filename');
            $table->string('storage_id');
			$table->string('path');
			$table->integer('magento_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kunden_attachments');
    }
}
