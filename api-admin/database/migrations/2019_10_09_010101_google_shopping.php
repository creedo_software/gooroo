<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GoogleShopping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spider_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('search');
            $table->longText('page');
            $table->timestamps();
        });
		
		Schema::create('spider_prices_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id');
			$table->integer('spider_log_id');
			$table->string('sku');
            $table->string('ean');
            $table->string('link', 800);
            $table->string('title');
            $table->string('article_title');
			$table->float('price');
            $table->float('tax');
            $table->json('parameters');
            $table->integer('levenshtein');
            $table->boolean('is_best_match');
            $table->boolean('is_lowest_price');
            $table->timestamps();
        });
		
		Schema::create('spider_prices_latest', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id');
			$table->integer('spider_log_id');
			$table->string('sku');
            $table->string('ean');
            $table->string('title');
            $table->string('link', 800);
            $table->string('article_title');
			$table->float('price');
            $table->float('tax');
            $table->json('parameters');
            $table->integer('levenshtein');
            $table->boolean('is_best_match');
            $table->boolean('is_lowest_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spider_logs');
		Schema::dropIfExists('spider_prices_history');
		Schema::dropIfExists('spider_prices_latest');
    }
}
