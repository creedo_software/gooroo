<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('me', 'UserController@me');

Route::post('/csv/import', 'CsvImportedController@import');

Route::get('/stocks/magento/list', 'StocksMagentoController@list');

Route::post('/migration/load/{action}', 'MigrationController@start');
Route::get('/migration/stats/{action}', 'MigrationController@stats');

Route::get('/migration/sync/{shop_id}', 'MigrationController@sync');

Route::get('/mapping/{shop_id}/{destination}/{entity}/list', 'MappingController@list');
Route::get('/mapping/{shop_id}/{destination}/{entity}/options', 'MappingController@options');
Route::post('/mapping/{shop_id}/{destination}/{entity}/{key}', 'MappingController@update');

Route::get('/mapping-article/{shop_id}/list', 'MappingArticleController@list');
Route::get('/mapping-article/{shop_id}/options', 'MappingArticleController@options');
Route::post('/mapping-article/{shop_id}/{key}', 'MappingArticleController@update');


Route::post('/media/info', 'ArticleMediaController@info');
Route::post('/media/import/{sku}/shop/{shop}', 'ArticleMediaController@upload');
Route::get('/media/export/{id}/{shop_id}', 'ArticleMediaController@export');
Route::get('/media/delete/{id}/{shop_id}', 'ArticleMediaController@delete');

Route::get('/order/sync/{id}', 'OrderController@sync');

Route::post('/ftp/list', 'FTPController@list');
Route::post('/ftp/load', 'FTPController@load');

Route::post('/kunde-attachment/sync', 'KundeAttachmentController@sync');

Route::get('/article/export/{id}/{shop_id}/', 'ArticleController@export');
Route::get('/article/spider/{id}/shop/{shop_id}', 'ArticleController@spider');

Route::get('/spider-price-latest/toggle-lock/{sku}/{google_id}', 'SpiderPriceLatestController@toggleLock');

Route::get('/category/{const}/shop/{shop_id}', 'CategoryController@show');
Route::post('/category/{const}/shop/{shop_id}', 'CategoryController@update');
Route::put('/category/{const}/shop/{shop_id}', 'CategoryController@update');

Route::get('/log/names', 'LogController@names');
Route::post('/test-soap/test', 'TestSOAPController@test');

Route::get('/kunde-integra/export-discounts/{id_integra}/{shop_id}', 'KundeIntegraController@discounts');
Route::get('/kunde-integra/generate-discounts/{id_integra}/{shop_id}', 'KundeIntegraController@generate');
Route::get('/kunde-integra/export-attachments/{id_integra}/{shop_id}', 'KundeIntegraController@attachments');
Route::get('/kunde-integra/export/{id_integra}/{shop_id}', 'KundeIntegraController@export');

Route::get('/spider-price-latest/csv', 'SpiderPriceLatestController@csv');


Route::apiResource('users', 'UserController');
Route::apiResource('article-access', 'ArticleAccessController');
Route::apiResource('kunde-address', 'KundeAddressController');
Route::apiResource('kunde-integra', 'KundeIntegraController');
Route::apiResource('kunde-magento', 'KundeMagentoController');
Route::apiResource('hersteller', 'HerstellerController');
Route::apiResource('kunde-attachment', 'KundeAttachmentController');
Route::apiResource('jobs', 'JobController');
Route::apiResource('log', 'LogController');
Route::apiResource('price-storage', 'PriceStorageController');
Route::apiResource('csv', 'CsvImportedController');
Route::apiResource('article-log', 'ArticleLogController');
Route::apiResource('article-stock', 'ArticleStockController');
Route::apiResource('article-price', 'ArticlePriceController');
Route::apiResource('spider-price-latest', 'SpiderPriceLatestController');
Route::apiResource('spider-price-history', 'SpiderPriceHistoryController');
Route::apiResource('spider-log', 'SpiderLogController');
Route::apiResource('price-kunde', 'PriceKundeController');
Route::apiResource('article-discount', 'ArticleDiscountController');
Route::apiResource('article-attribute', 'ArticleAttributeController');
Route::apiResource('article-attribute-integra', 'ArticleAttributeIntegraController');
Route::apiResource('order-attribute', 'OrderAttributeController');
Route::apiResource('order', 'OrderController');
Route::apiResource('article', 'ArticleController');
Route::apiResource('shop', 'ShopController');
Route::apiResource('category', 'CategoryController');


