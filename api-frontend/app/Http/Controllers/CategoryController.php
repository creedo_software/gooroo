<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    
    public function tree(Request $request)
    {
        return response()->json(Category::get()->toTree());    
    }
}
