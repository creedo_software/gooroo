<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class GeneratorController extends Controller
{
    public function categories(Request $request)
    {
        $node = Category::create([
            'name' => 'House',
            'description' => 'house',
            'image' => 'categories/house.png',
            'children' => [
                ['name' => 'house sub category 1', 'description' => 'house sub category 1', 'children' => []],
                ['name' => 'house sub category 2', 'description' => 'house sub category 2', 'children' => []],
                ['name' => 'house sub category 3', 'description' => 'house sub category 3', 'children' => []],
                ['name' => 'house sub category 4', 'description' => 'house sub category 4', 'children' => []],
                ['name' => 'house sub category 5', 'description' => 'house sub category 5', 'children' => []],
                ['name' => 'house sub category 6', 'description' => 'house sub category 6', 'children' => []],
                ['name' => 'house sub category 7', 'description' => 'house sub category 7', 'children' => []],
                ['name' => 'house sub category 8', 'description' => 'house sub category 8', 'children' => []],
                ['name' => 'house sub category 9', 'description' => 'house sub category 9', 'children' => []],
                ['name' => 'house sub category 10', 'description' => 'house sub category 10', 'children' => []],
                ['name' => 'house sub category 11', 'description' => 'house sub category 11', 'children' => []],
                ['name' => 'house sub category 12', 'description' => 'house sub category 12', 'children' => []],
            ],
        ]);
        $node = Category::create([
            'name' => 'Delivery',
            'description' => 'delivery',
            'image' => 'categories/delivery.png',
            'children' => [
                ['name' => 'delivery sub category 1', 'description' => 'delivery sub category 1', 'children' => []],
                ['name' => 'delivery sub category 2', 'description' => 'delivery sub category 2', 'children' => []],
                ['name' => 'delivery sub category 3', 'description' => 'delivery sub category 3', 'children' => []],
                ['name' => 'delivery sub category 4', 'description' => 'delivery sub category 4', 'children' => []],
                ['name' => 'delivery sub category 5', 'description' => 'delivery sub category 5', 'children' => []],
                ['name' => 'delivery sub category 6', 'description' => 'delivery sub category 6', 'children' => []],
                ['name' => 'delivery sub category 7', 'description' => 'delivery sub category 7', 'children' => []],
                ['name' => 'delivery sub category 8', 'description' => 'delivery sub category 8', 'children' => []],
                ['name' => 'delivery sub category 9', 'description' => 'delivery sub category 9', 'children' => []],
                ['name' => 'delivery sub category 10', 'description' => 'delivery sub category 10', 'children' => []],
                ['name' => 'delivery sub category 11', 'description' => 'delivery sub category 11', 'children' => []],
                ['name' => 'delivery sub category 12', 'description' => 'delivery sub category 12', 'children' => []],
            ],
        ]);
        $node = Category::create([
            'name' => 'Freelance',
            'description' => 'freelance',
            'image' => 'categories/freelance.png',
            'children' => [
                ['name' => 'freelance sub category 1', 'description' => 'freelance sub category 1', 'children' => []],
                ['name' => 'freelance sub category 2', 'description' => 'freelance sub category 2', 'children' => []],
                ['name' => 'freelance sub category 3', 'description' => 'freelance sub category 3', 'children' => []],
                ['name' => 'freelance sub category 4', 'description' => 'freelance sub category 4', 'children' => []],
                ['name' => 'freelance sub category 5', 'description' => 'freelance sub category 5', 'children' => []],
                ['name' => 'freelance sub category 6', 'description' => 'freelance sub category 6', 'children' => []],
                ['name' => 'freelance sub category 7', 'description' => 'freelance sub category 7', 'children' => []],
                ['name' => 'freelance sub category 8', 'description' => 'freelance sub category 8', 'children' => []],
                ['name' => 'freelance sub category 9', 'description' => 'freelance sub category 9', 'children' => []],
                ['name' => 'freelance sub category 10', 'description' => 'freelance sub category 10', 'children' => []],
                ['name' => 'freelance sub category 11', 'description' => 'freelance sub category 11', 'children' => []],
                ['name' => 'freelance sub category 12', 'description' => 'freelance sub category 12', 'children' => []],
            ],
        ]);
        $node = Category::create([
            'name' => 'Tutors',
            'description' => 'tutors',
            'image' => 'categories/tutors.png',
            'children' => [
                ['name' => 'tutors sub category 1', 'description' => 'tutors sub category 1', 'children' => []],
                ['name' => 'tutors sub category 2', 'description' => 'tutors sub category 2', 'children' => []],
                ['name' => 'tutors sub category 3', 'description' => 'tutors sub category 3', 'children' => []],
                ['name' => 'tutors sub category 4', 'description' => 'tutors sub category 4', 'children' => []],
                ['name' => 'tutors sub category 5', 'description' => 'tutors sub category 5', 'children' => []],
                ['name' => 'tutors sub category 6', 'description' => 'tutors sub category 6', 'children' => []],
                ['name' => 'tutors sub category 7', 'description' => 'tutors sub category 7', 'children' => []],
                ['name' => 'tutors sub category 8', 'description' => 'tutors sub category 8', 'children' => []],
                ['name' => 'tutors sub category 9', 'description' => 'tutors sub category 9', 'children' => []],
                ['name' => 'tutors sub category 10', 'description' => 'tutors sub category 10', 'children' => []],
                ['name' => 'tutors sub category 11', 'description' => 'tutors sub category 11', 'children' => []],
                ['name' => 'tutors sub category 12', 'description' => 'tutors sub category 12', 'children' => []],
            ],
        ]);
        $node = Category::create([
            'name' => 'Business',
            'description' => 'business',
            'image' => 'categories/business.png',
            'children' => [
                ['name' => 'business sub category 1', 'description' => 'business sub category 1', 'children' => []],
                ['name' => 'business sub category 2', 'description' => 'business sub category 2', 'children' => []],
                ['name' => 'business sub category 3', 'description' => 'business sub category 3', 'children' => []],
                ['name' => 'business sub category 4', 'description' => 'business sub category 4', 'children' => []],
                ['name' => 'business sub category 5', 'description' => 'business sub category 5', 'children' => []],
                ['name' => 'business sub category 6', 'description' => 'business sub category 6', 'children' => []],
                ['name' => 'business sub category 7', 'description' => 'business sub category 7', 'children' => []],
                ['name' => 'business sub category 8', 'description' => 'business sub category 8', 'children' => []],
                ['name' => 'business sub category 9', 'description' => 'business sub category 9', 'children' => []],
                ['name' => 'business sub category 10', 'description' => 'business sub category 10', 'children' => []],
                ['name' => 'business sub category 11', 'description' => 'business sub category 11', 'children' => []],
                ['name' => 'business sub category 12', 'description' => 'business sub category 12', 'children' => []],
            ],
        ]);
        return response()->json(['imported' => 'success']); 
    }
    
}
