<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller
{
      public function homepage(Request $request)
      {
          return response()->json(['homepage' => []]);    
      }
}
