<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getMyUser(Request $request){
        $user = JWTAuth::authenticate($request->bearerToken());

        return response()->json(['user' => User::find($user->id)]);
    }
	
	public function update(Request $request){
		$token = JWTAuth::authenticate($request->bearerToken());
		
		$user = User::find($token->id);
    	$user->update($request->all());
		
		return response()->json(['user' => $user]);
	}
}
