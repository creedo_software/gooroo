<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
	
	public function getMyWorker(Request $request){
		$token = JWTAuth::authenticate($request->bearerToken());
		
		$worker = \App\Models\Worker::findOrCreateForUser($token->id);		
		return response()->json(['worker' => $worker]);
	}
	
    public function updateMyWorker(Request $request){
		$token = JWTAuth::authenticate($request->bearerToken());
		
		$worker = \App\Models\Worker::findOrCreateForUser($token->id);
    	$worker->update($request->all());
		
		return response()->json(['worker' => $worker]);
	}
	
	public function search(Request $request){
		$workers = \App\Models\Worker::where([['workers.id', '>', 0]])
			->select(
				'users.first_name', 'users.last_name', 'workers.hourly_rate',
				'users.last_logged_at', 'users.avatar', 'users.city',
				'users.country', 'users.address', 
				'workers.id', 'workers.summary', 'workers.qualification',
				'workers.rating', 'workers.total_hours', 'workers.completed_jobs'
			)
			->join('users', 'users.id', '=', 'workers.user_id')
			->get();
		return response()->json(['workers' => $workers]);
	}
}
