<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model {
    use NodeTrait;
    use HasFactory;

	protected $fillable = [
        'name', 
        'image',
        'description'
    ];
}