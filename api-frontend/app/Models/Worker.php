<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Worker extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id', 'status', 'summary', 'rating', 'total_hours',
        'hourly_rate', 'qualification', 'completed_jobs'
    ];
    
    public function user(){
        return $this->hasOne(User::class);
    }
    
    public static function findOrCreateForUser($user_id){
        $exists = Worker::where('user_id', $user_id)->first();
        if($exists){
            return $exists;
        }
        
        return Worker::create([
            'user_id' => $user_id,
            'status' => 1
        ]);
    }
}