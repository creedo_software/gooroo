<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image')->nullable(true)->default(null);
            $table->string('description')->nullable(true)->default(null);
            $table->boolean('enabled')->default(true);
            $table->timestamps();
            
            NestedSet::columns($table);
        });
    }


    public function down()
    {
        Schema::drop('categories');
    }
}
