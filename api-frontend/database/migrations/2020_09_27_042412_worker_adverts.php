<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WorkerAdverts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_adverts', function (Blueprint $table) {
            $table->id();
            $table->integer('worker_id');
            $table->integer('worker_advert_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->decimal('price', 8, 2);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_adverts');
    }
}
