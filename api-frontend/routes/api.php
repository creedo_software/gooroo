<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\GeneratorController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
  
Route::group(['middleware' => 'jwt.auth'], function () {
	Route::get('/logout', [AuthController::class, 'logout']);
	Route::get('/me', [UserController::class, 'getMyUser']);
    Route::get('/me/worker', [WorkerController::class, 'getMyWorker']);
    Route::post('/user', [UserController::class, 'update']);
    Route::post('/me/worker', [WorkerController::class, 'updateMyWorker']);
});

Route::get('/info/homepage', [InfoController::class, 'homepage']);
Route::get('/category/tree', [CategoryController::class, 'tree']);
Route::get('/workers', [WorkerController::class, 'search']);



Route::get('/generator/categories', [GeneratorController::class, 'categories']);


