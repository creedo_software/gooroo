import * as types from './index';

export const loginAction = (params) => {
    return {
        type: types.LOGIN_REQUEST,
		params: params
    }
};

export const registerAction = (params) => {
    return {
        type: types.REGISTER_REQUEST,
		params: params
    }
};


export const meAction = (params) => {
    return {
        type: types.ME_REQUEST,
		params: params
    }
};