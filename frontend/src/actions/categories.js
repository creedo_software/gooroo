import * as types from './index';

export const categoriesTreeAction = (params) => {
    return {
        type: types.CATEGORIES_TREE_REQUEST,
		params: params
    }
};