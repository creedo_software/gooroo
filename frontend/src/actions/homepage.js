import * as types from './index';

export const homepageInfoAction = (params) => {
    return {
        type: types.HOMEPAGE_INFO_REQUEST,
		params: params
    }
};