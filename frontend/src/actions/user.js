import * as types from './index';

export const updateProfileAction = (params) => {
    return {
        type: types.UPDATE_PROFILE_REQUEST,
		params: params
    }
};
