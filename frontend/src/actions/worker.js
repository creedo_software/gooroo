import * as types from './index';

export const updateMyWorkerAction = (params) => {
    return {
        type: types.UPDATE_MY_WORKER_REQUEST,
		params: params
    }
}

export const getMyWorkerAction = (params) => {
    return {
        type: types.GET_MY_WORKER_REQUEST,
		params: params
    }
}

export const findWorkersAction = (params) => {
    return {
        type: types.FIND_WORKERS_REQUEST,
		params: params
    }
}

export const getWorkerAction = (params) => {
    return {
        type: types.GET_WORKER_REQUEST,
		params: params
    }
}