import React, { Component } from 'react';
import { Redirect } from "react-router-dom"; 

import Authmanager from 'react-authmanager';

Authmanager.addGuard('loggedGuard', function(user, props, next) {
	//if (user.loading)
	//	return (<p>LOADING USER</p>); // render a loading component if user is currently fetched from the server

	if (Authmanager.getToken())
		return next(); // render the component if user is not loading and is logged

	return (<Redirect to="/login" />); // render a login component by default (if user is fetched from the server but not logged)
});

export const Conf = Authmanager.conf