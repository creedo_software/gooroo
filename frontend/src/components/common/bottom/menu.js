import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from "react-router-dom";

import YouTubeIcon from '@material-ui/icons/YouTube';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: '20px',
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
	},
	menuButton: {
		marginRight: theme.spacing(2),
		marginTop: theme.spacing(2),
	},
	menuLink: {
		textDecoration: 'none'
	}
}))

export default function BottomMenu() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<div style={{width: '30%'}}>
				<Link to="/faq" className={classes.menuLink}>
					<Button color="secondary" variant="contained" className={classes.menuButton}>
						FAQ
					</Button>
				</Link>
				<Link to="/terms" className={classes.menuLink}>
					<Button color="secondary" variant="contained" className={classes.menuButton}>
						Terms & Conditions
					</Button>
				</Link>
				<Link to="/about-us" className={classes.menuLink}>
					<Button color="secondary" variant="contained" className={classes.menuButton}>
						About Us
					</Button>
				</Link>
				<Link to="/privacy-policy" className={classes.menuLink}>
					<Button color="secondary" variant="contained" className={classes.menuButton}>
						Privacy Policy
					</Button>
				</Link>
				<Link to="/help" className={classes.menuLink}>
					<Button color="secondary" variant="contained" className={classes.menuButton}>
						Help
					</Button>
				</Link>
				<Link to="/contact-us" className={classes.menuLink}>
					<Button color="secondary" variant="contained" className={classes.menuButton}>
						Contact Us
					</Button>
				</Link>
			</div>
			
			<div style={{width: '30%'}}>
				<IconButton aria-label="Facebook" className={classes.margin}>
					<FacebookIcon fontSize="large" style={{fontSize: "3em"}} />
				</IconButton>
				<IconButton aria-label="Instagram" className={classes.margin}>
					<InstagramIcon fontSize="large" style={{fontSize: "3em"}} />
				</IconButton>
				<IconButton aria-label="YouTube" className={classes.margin}>
					<YouTubeIcon fontSize="large" style={{fontSize: "3em"}} />
				</IconButton>
			</div>
		</div>
	)
}