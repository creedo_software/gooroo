import React, { Component } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from "react-router-dom";
import style from "./menu.less";
import { meAction } from '../../../actions/auth';
import Authmanager from 'react-authmanager';

class HeaderMenu extends Component {

	componentDidMount(){
        if(
			Authmanager.getToken() 
		){
			this.props.dispatch(meAction());
		}
    }
	
	componentDidUpdate(prevProps) {
		if(
			Authmanager.getToken() 
			&& !this.props.response.me?.response?.user
		){
		//	this.props.dispatch(meAction());
		}
	}
	
	render = () => {			
		const user = this.props.response.me?.response ? this.props.response.me?.response?.user : null
			
		return (
			<AppBar position="static" color="transparent" className={style.root}>
				<Toolbar className={style.toolbar}>
					<div>
						<Link to="/" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								Home
							</Button>
						</Link>
						<Link to="/about-us" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								About Us
							</Button>
						</Link>
						<Link to="/how-it-works" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								How it works
							</Button>
						</Link>
					</div>
					<Typography variant="h3" className={style.title}>
						GooRoo
					</Typography>
					<div>
						<Link to="/start" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								Start providing services
							</Button>
						</Link>
						{!user && (<><Link to="/sign-up" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								Sign Up
							</Button>
						</Link>
						<Link to="/login" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								Login
							</Button>
						</Link></>)}
						{user && (<Link to="/profile" className={style.menuLink}>
							<Button color="secondary" variant="contained" className={style.menuButton}>
								{user.first_name} {user.last_name}
							</Button>
						</Link>)}
					</div>
				</Toolbar>
			</AppBar>
		);
	}
}

const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(HeaderMenu));


