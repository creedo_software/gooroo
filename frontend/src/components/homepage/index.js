import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import HouseIcon from '@material-ui/icons/House';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import ComputerIcon from '@material-ui/icons/Computer';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import { Link } from "react-router-dom";
import { Typography } from '@material-ui/core';
import { homepageInfoAction } from '../../actions/homepage';
import { categoriesTreeAction } from '../../actions/categories';

class Homepage extends Component {

    state = {
        categories: [],
        selectedCategory: null
    }

    componentDidUpdate(prevProps) {
        if (
            !prevProps.response.categories.response
            && this.props.response.categories.response
        ) {
            this.setState({
                categories: this.props.response.categories.response,
                selectedCategory: this.props.response.categories.response[0]
            })
        }
    }

    componentDidMount(){
        this.props.dispatch(homepageInfoAction({
            param1: 'param-value'
        }));
        
        this.props.dispatch(categoriesTreeAction({
            param1: 'param-value'
        }));
    }
    
    topCategoryIcon = category => {
        if(category.name.toLowerCase() == 'house'){
            return (<HouseIcon fontSize="large" style={{fontSize: "3em"}} />)
        }
        if(category.name.toLowerCase() == 'delivery'){
            return (<LocalShippingIcon fontSize="large" style={{fontSize: "3em"}} />)
        }
        if(category.name.toLowerCase() == 'freelance'){
            return (<ComputerIcon fontSize="large" style={{fontSize: "3em"}} />)
        }
        if(category.name.toLowerCase() == 'tutors'){
            return (<MenuBookIcon fontSize="large" style={{fontSize: "3em"}} />)
        }
        if(category.name.toLowerCase() == 'business'){
            return (<BusinessCenterIcon fontSize="large" style={{fontSize: "3em"}} />)
        }
        
        return (<></>)
    }
    
    topCategories = () => {
        return (
            <Grid
                container
                item
                direction="row"
                justify="center"
                alignItems="center"
                spacing={4}
            >
                {this.state.categories.map(category => (
                    <Grid 
                        key={category.id}
                        item
                        style={{cursor: 'pointer'}}
                        onClick={() => this.setState({selectedCategory: category})}
                    >
                        <IconButton aria-label={category.name}>
                            {this.topCategoryIcon(category)}
                        </IconButton>
                        <Typography variant="h5" gutterBottom>
                            {category.name}
                        </Typography>
                    </Grid>
                ))}
            </Grid>
        )
    }
    
    subcategories = () => (
        <Grid
            item
            container
            direction="row"
            spacing={2}
        >
            {this.state.categories.find(category => category.id == this.state.selectedCategory.id).children.map(category => (
                <Grid item xs={3} key={category.id}>
                    <Link to={"/workers?category_id=" + category.id}>
                        <Typography variant="h6" gutterBottom>
                            {category.name}
                        </Typography>
                    </Link>
                </Grid>
            ))}
        </Grid>
    )
	
    render = () => {        
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '40px 0px'}}
            >
                <Typography variant="h5" gutterBottom>
                    Select a category or search a service:
                </Typography>
                <Grid
                    container
                    item
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <TextField label="Standard" />
                    <Button color="secondary" variant="contained" >
                        Search
                    </Button>
                </Grid>
                {this.topCategories()}
                {this.state.selectedCategory && this.subcategories()}
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(Homepage));