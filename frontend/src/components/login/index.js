import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import HouseIcon from '@material-ui/icons/House';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import ComputerIcon from '@material-ui/icons/Computer';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';
import { loginAction } from '../../actions/auth';
import Authmanager from 'react-authmanager';

class Login extends Component {

    state = {
        email: '',
        password: '',
    }

    componentDidUpdate(prevProps) {
        console.log('componentDidUpdate', this.props)
        if(this.props.response.login 
            && this.props.response.login.response 
            && this.props.response.login.response.token
        ){
            Authmanager.setToken(this.props.response.login.response.token);
            this.props.history.push('/dashboard')
        }
    }

    componentDidMount(){
        
    }
    
    submit = () => {
        this.props.dispatch(loginAction({
            'email': this.state.email,
            'password': this.state.password
        }));
    }
    
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    
    render = () => {
        const { message } = this.props.response.login.response ? this.props.response.login.response : {}
        
        return (
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '0px auto', width: '40%'}}
            >
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    spacing={4}
                    style={{margin: '40px 0px'}}
                >
                    <Typography variant="h5" gutterBottom>
                        Login
                    </Typography>
                    
                    <TextField
                        label="Email address:"
                        name="email"
                        className={style.inputFullWidth}
                        value={this.state.email}
                        onChange={this.handleChange}
                    />
                    
                    <TextField
                        label="Password:"
                        name="password"
                        type="password"
                        className={style.inputFullWidth}
                        value={this.state.password}
                        onChange={this.handleChange}
                    />
                    
                    {message ? (
                        <FormHelperText style={{color: 'red'}}>{message}</FormHelperText>
                    ) : ''}
                    <br />
                    
                    <Button color="secondary" variant="contained" onClick={this.submit}>
                        Login
                    </Button>
                </Grid>
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(Login));