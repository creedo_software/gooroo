import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Authmanager from 'react-authmanager';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';
import Sidebar from "../sidebar";
import { updateProfileAction } from '../../../actions/user';


class Basic extends Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
    }

    componentDidUpdate(prevProps) {
        if(
            this.props.response?.me?.response?.user
            && !prevProps.response?.me?.response?.user
        ){
            this.setState({
                firstName: this.props.response.me.response.user.first_name,
                lastName: this.props.response.me.response.user.last_name,
                email: this.props.response.me.response.user.email,
            })
        }
    }

    componentDidMount(){
        
    }
    
    submit = () => {
        this.props.dispatch(updateProfileAction({
            'first_name': this.state.firstName,
            'last_name': this.state.lastName,
            'email': this.state.email,
        }));
    }
    
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    
    render = () => {    
        const { error } = this.props.response.user?.response?.basic || {}
            
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '40px 20% 0px 20%', width: 'auto'}}
            >
                <Typography variant="h5" gutterBottom>
                    Profile
                </Typography>
                
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={4}
                >
                    <div style={{width: '25%'}}>
                        <Sidebar />
                    </div>
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        spacing={4}
                        style={{width: '50%', padding: '0px 40px'}}
                    >
                        <TextField
                            label="First name:"
                            name="firstName"
                            className={style.inputFullWidth}
                            value={this.state.firstName}
                            onChange={this.handleChange}
                            {
                                ...(error && error.first_name ? {
                                    helperText: error.first_name,
                                    error: true
                                }: {})
                            }
                        />
                        
                        <TextField
                            label="Last name:"
                            name="lastName"
                            className={style.inputFullWidth}
                            value={this.state.lastName}
                            onChange={this.handleChange}
                            {
                                ...(error && error.last_name ? {
                                    helperText: error.last_name,
                                    error: true
                                }: {})
                            }
                        />
                        
                        <TextField
                            label="Email address:"
                            name="email"
                            className={style.inputFullWidth}
                            value={this.state.email}
                            onChange={this.handleChange}
                            {
                                ...(error && error.email ? {
                                    helperText: error.email,
                                    error: true
                                }: {})
                            }
                        />
                        <br />
                        <Button color="secondary" variant="contained" onClick={this.submit}>
    						Update
    					</Button>
                    </Grid>
                    <div style={{width: '25%'}}>
                    </div>
                </Grid>
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(Authmanager.withGuard('loggedGuard')(withRouter(Basic)));