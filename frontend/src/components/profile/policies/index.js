import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Authmanager from 'react-authmanager';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';

class Policies extends Component {

    state = {
       
    }

    componentDidUpdate(prevProps) {
        
    }

    componentDidMount(){
        
    }
    
    render = () => {        
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '40px 0px'}}
            >
                <Typography variant="h5" gutterBottom>
                    Profile
                </Typography>
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(Authmanager.withGuard('loggedGuard')(withRouter(Policies)));