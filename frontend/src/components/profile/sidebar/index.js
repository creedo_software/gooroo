import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Authmanager from 'react-authmanager';
import { Link } from "react-router-dom";
import { Typography } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

class Sidebar extends Component {

    state = {
       
    }

    componentDidUpdate(prevProps) {
        
    }

    componentDidMount(){
        
    }
    
    render = () => {   
        return (
            <List component="nav">
                <ListItem>
                    <Link to="/profile/basic" className={style.menuLink}>
                        <Button color="secondary" variant="contained" className={style.menuButton}>
                            General Information
                        </Button>
                    </Link>
                </ListItem>
                <ListItem>
                    <Link to="/profile/work" className={style.menuLink}>
                        <Button color="secondary" variant="contained" className={style.menuButton}>
                            Teacher Settings
                        </Button>
                    </Link>
                </ListItem>
                <ListItem>
                    <Link to="/profile/policies" className={style.menuLink}>
                        <Button color="secondary" variant="contained" className={style.menuButton}>
                            Policies
                        </Button>
                    </Link>
                </ListItem>
                <ListItem>
                    <Link to="/profile/availability" className={style.menuLink}>
                        <Button color="secondary" variant="contained" className={style.menuButton}>
                            Availability
                        </Button>
                    </Link>
                </ListItem>
            </List>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(Authmanager.withGuard('loggedGuard')(withRouter(Sidebar)));