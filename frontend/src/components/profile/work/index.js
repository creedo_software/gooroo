import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Authmanager from 'react-authmanager';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';
import Sidebar from "../sidebar";
import { updateMyWorkerAction, getMyWorkerAction } from '../../../actions/worker';

class Work extends Component {

    state = {
        userId: '',
        hourlyRate: '',
        summary: '',
        qualification: ''
    }

    componentDidUpdate(prevProps) {
        const { my_worker } = this.props.response.worker
        if(
            my_worker
            && my_worker.worker
            && !this.state.userId
        ){
            this.setState({
                userId: my_worker.worker.user_id,
                hourlyRate: my_worker.worker.hourly_rate || '',
                summary: my_worker.worker.summary || '',
                qualification: my_worker.worker.qualification || ''
            })
        }
    }

    componentDidMount(){
        this.props.dispatch(getMyWorkerAction())
    }
    
    submit = () => {
        this.props.dispatch(updateMyWorkerAction({
            hourly_rate: this.state.hourlyRate,
            summary: this.state.summary,
            qualification: this.state.qualification
        }));
    }
    
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    
    render = () => {
        const { error } = this.props.response.worker?.response?.work || {}
           
        return (
            <Grid
                container
                direction="column"
                justify="center" 
                alignItems="center"
                spacing={4}
                style={{margin: '40px 20% 0px 20%', width: 'auto'}}
            >
                <Typography variant="h5" gutterBottom style={{ width: '100%', textAlign: 'center'}}>
                    My profile
                </Typography>
                
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                    spacing={4}
                    style={{margin: '0'}}
                >
                    <div style={{width: '25%'}}>
                        <Sidebar />
                    </div>
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        spacing={4}
                        style={{width: '50%', padding: '0px 40px', margin: '0'}}
                    >
                        <TextField
                            label="Hourly Rate:"
                            name="hourlyRate"
                            className={style.inputFullWidth}
                            value={this.state.hourlyRate}
                            onChange={this.handleChange}
                            {
                                ...(error && error.hourly_rate ? {
                                    helperText: error.hourly_rate,
                                    error: true
                                }: {})
                            }
                        />
                        
                        <TextField
                            label="Summary:"
                            name="summary"
                            multiline
                            rows="10"
                            className={style.inputFullWidth}
                            value={this.state.summary}
                            onChange={this.handleChange}
                            {
                                ...(error && error.summary ? {
                                    helperText: error.summary,
                                    error: true
                                }: {})
                            }
                        />
                        
                        <TextField
                            label="Qualification:"
                            name="qualification"
                            multiline
                            rows="10"
                            className={style.inputFullWidth}
                            value={this.state.qualification}
                            onChange={this.handleChange}
                            {
                                ...(error && error.qualification ? {
                                    helperText: error.qualification,
                                    error: true
                                }: {})
                            }
                        />
                        
                        <br />
                        <Button color="secondary" variant="contained" onClick={this.submit}>
    						Update
    					</Button>
                    </Grid>
                    <div style={{width: '25%'}}>
                    </div>
                </Grid>
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(Authmanager.withGuard('loggedGuard')(withRouter(Work)));