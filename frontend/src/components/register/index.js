import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import Authmanager from 'react-authmanager';
import { Typography } from '@material-ui/core';
import { registerAction } from '../../actions/auth';

class Register extends Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        passwordConfirm: ''
    }

    componentDidUpdate(prevProps) {
        if(this.props.response.register 
            && this.props.response.register.response 
            && this.props.response.register.response.token
        ){
            Authmanager.setToken(this.props.response.register.response.token);
            this.props.history.push('/dashboard')
        }
    }

    componentDidMount(){
        
    }
    
    submit = () => {
        this.props.dispatch(registerAction({
            'first_name': this.state.firstName,
            'last_name': this.state.lastName,
            'email': this.state.email,
            'password': this.state.password,  
            'c_password': this.state.passwordConfirm,  
            'agree': this.state.agree
        }));
    }
    
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    
    handleChangeAgree = (event) => {
        this.setState({
            agree: event.target.checked 
        });
    }
	
    render = () => {        
        const { error } = this.props.response.register.response ? this.props.response.register.response : {}
        
        return (                
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '0px auto', width: '40%'}}
            >
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    spacing={4}
                    style={{margin: '40px 0px', width: '100%'}}
                >
                    <Typography variant="h5" gutterBottom>
                        Sign Up as a customer
                    </Typography>
                    
                    <TextField
                        label="First name:"
                        name="firstName"
                        className={style.inputFullWidth}
                        value={this.state.firstName}
                        onChange={this.handleChange}
                        {
                            ...(error && error.first_name ? {
                                helperText: error.first_name,
                                error: true
                            }: {})
                        }
                    />
                    
                    <TextField
                        label="Last name:"
                        name="lastName"
                        className={style.inputFullWidth}
                        value={this.state.lastName}
                        onChange={this.handleChange}
                        {
                            ...(error && error.last_name ? {
                                helperText: error.last_name,
                                error: true
                            }: {})
                        }
                    />
                    
                    <TextField
                        label="Email address:"
                        name="email"
                        className={style.inputFullWidth}
                        value={this.state.email}
                        onChange={this.handleChange}
                        {
                            ...(error && error.email ? {
                                helperText: error.email,
                                error: true
                            }: {})
                        }
                    />
                    
                    <TextField
                        label="Password:"
                        name="password"
                        type="password"
                        className={style.inputFullWidth}
                        value={this.state.password}
                        onChange={this.handleChange}
                        {
                            ...(error && error.password ? {
                                helperText: error.password,
                                error: true
                            }: {})
                        }
                    />
                    
                    <TextField
                        label="Confirm password:"
                        name="passwordConfirm"
                        type="password"
                        className={style.inputFullWidth}
                        value={this.state.passwordConfirm}
                        onChange={this.handleChange}
                        {
                            ...(error && error.c_password ? {
                                helperText: error.c_password,
                                error: true
                            }: {})
                        }
                    />
                    
                    <FormControlLabel
                        control={<Checkbox checked={this.state.agree} onChange={this.handleChangeAgree} name="agree" />}
                        label={"By clicking \"Sign Up\" button, you agree with our Terms & conditions and Priivacy Policy"}
                    />
                    {error && error.agree ? (
                        <FormHelperText style={{color: 'red'}}>{error.agree}</FormHelperText>
                    ) : ''}
                    
                    <Button color="secondary" variant="contained" onClick={this.submit}>
						Sign Up
					</Button>
                </Grid>
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(Register));