import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

class HowItWorks extends Component {

	render = () => {
        console.log('RESPONSE', this.props.response)
        
        return (
			<Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '40px 0px'}}
            >
                <Typography variant="h5" gutterBottom>
                	How It Works
                </Typography>
				
				<Grid item>
					<Typography variant="body1" gutterBottom>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ac elementum massa. Vivamus semper purus magna, a commodo purus aliquet sed. Proin aliquet viverra lorem. Nulla velit ligula, vehicula eget ligula id, dictum porttitor urna. Donec vitae mattis nisl. Aliquam vestibulum ornare mauris, id cursus nulla convallis quis. Nam id semper est, vitae euismod lacus. Praesent molestie, ex vel rhoncus pretium, mauris odio tristique neque, vitae sollicitudin sem orci sodales lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum lobortis venenatis ligula et volutpat. Etiam eu neque turpis. Phasellus ut nunc convallis nisi pellentesque vestibulum at ut tortor. Aenean vel consectetur leo. Nunc aliquet ligula magna, sit amet ornare urna tempus placerat. Vivamus diam felis, porta a tempus et, ultrices sollicitudin neque. Sed molestie eleifend lorem quis placerat.
					</Typography>
				</Grid>
				
				<Grid item>
					<Typography variant="body1" gutterBottom>
						Ut ac mollis arcu. Fusce suscipit vulputate velit mollis facilisis. Fusce pretium tempor dolor. Pellentesque id turpis eros. In ac tincidunt lacus. Maecenas a leo et leo gravida suscipit euismod eget nunc. Sed vehicula mollis velit et luctus. Nam dignissim nisl tortor, vel convallis neque imperdiet vel. Nulla lobortis at enim mollis condimentum. Aenean a sodales nisl. Etiam eu fermentum ipsum.
					</Typography>
				</Grid>
				
				<Grid item>
					<Typography variant="body1" gutterBottom>
						Ut fringilla magna id mi commodo scelerisque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed et varius lectus. Aliquam imperdiet ante purus, eget suscipit lorem malesuada sit amet. Aliquam tincidunt ex at consectetur varius. Aenean ante magna, finibus ornare arcu a, tristique tincidunt libero. Curabitur rhoncus placerat tellus, non suscipit justo malesuada a. Vivamus iaculis nunc nec odio sagittis fermentum. In vehicula sapien ac mauris interdum consectetur. Sed ac lacinia nunc, in tincidunt arcu. Sed semper magna felis, at vestibulum purus sodales ac. Pellentesque vel lacus non mi volutpat eleifend. Maecenas eu imperdiet neque. Suspendisse non rhoncus ligula, sed malesuada est. Sed consectetur metus id mattis accumsan. Vestibulum eu vestibulum tortor, a varius purus.
					</Typography>
				</Grid>
				
				<Grid item>
					<Typography variant="body1" gutterBottom>
						Nulla in est dui. Maecenas vulputate justo turpis, a sodales tortor ultrices at. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu eros eget sem maximus fringilla at id enim. In non dictum felis. Nam sed laoreet lectus. Etiam fermentum convallis lectus sagittis rhoncus. Cras quis felis sed quam vehicula maximus. Integer tristique dui turpis, eget iaculis lorem euismod nec. Integer nec augue eu nibh facilisis pellentesque. Sed sed condimentum diam. Curabitur non ultricies arcu. Sed quis quam id neque suscipit cursus sed non nisi.
					</Typography>
				</Grid>
				
				<Grid item>
					<Typography variant="body1" gutterBottom>
						Nulla sodales iaculis justo, mollis porta ex eleifend non. In commodo viverra rutrum. Sed pellentesque diam eget velit volutpat ultrices. Etiam aliquet, nibh non scelerisque facilisis, turpis erat convallis ipsum, eu auctor libero lectus at sapien. Maecenas accumsan interdum eleifend. Nam ac lectus vel sem eleifend rutrum eget vel magna. Nam quis est eu dui egestas tincidunt nec dapibus lorem. Duis id nunc mollis, varius nisl quis, placerat tellus. Integer augue mauris, viverra et fringilla et, sollicitudin tristique ante. Vivamus gravida pharetra ante eu convallis. Vestibulum sed accumsan eros. Nullam maximus sit amet velit vel lobortis. Suspendisse nec eros at dui molestie aliquet. Phasellus nec arcu diam.
					</Typography>
				</Grid>
			</Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(HowItWorks));