import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';
import { findWorkersAction } from '../../actions/worker';
import queryString from 'query-string';
import Worker from './worker'

class Workers extends Component {

    state = {
        
    }

    componentDidUpdate(prevProps) {
        
    }

    componentDidMount(){
        let params = queryString.parse(this.props.location.search)
        this.props.dispatch(findWorkersAction({
            categoryId: params.category_id
        }));
    }
    
    render = () => {   
        const workers = this.props.response.worker?.find?.workers || []
        console.log(workers, this.props.response.worker)
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '40px 0px'}}
            >
                <Typography variant="h5" gutterBottom>
                    Specialists:
                </Typography>
                <Grid
                    container
                    item
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <TextField label="Enter the name, category, skills or qualification" />
                    <Button color="secondary" variant="contained" >
                        Find
                    </Button>
                </Grid>
                {workers.map(worker => (<Worker key={worker.id} worker={worker} />))}
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(Workers));