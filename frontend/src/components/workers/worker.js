import React, { Component } from 'react'
import style from "./index.less";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import Rating from '@material-ui/lab/Rating';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';

class Worker extends Component {

    state = {
        
    }

    componentDidUpdate(prevProps) {
        
    }

    componentDidMount(){
        
    }
    
    render = () => { 
		const { worker } = this.props  
        const categories = worker.categories.split(';')
        
        return (
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={4}
                style={{margin: '0px 0px'}}
            >
                <Grid
                    container
                    item
                    direction="column"
                    justify="center"
                    alignItems="center"
                >
                    <Avatar src={worker.avatar} />
                    <Typography variant="body1" gutterBottom>
                        online was 9 mins
                    </Typography>
                    <Typography variant="body1" gutterBottom>
	                    {worker.hourly_rate} EUR / hour
	                </Typography>
                    <Grid
                        container
                        item
                        direction="row"
                        justify="space-between"
                        alignItems="center"
                    >
                        {categories.map(category => (<Chip label={category} />))}
                    </Grid>
                    <Typography variant="body1" gutterBottom>
	                    {worker.summary}
	                </Typography>
                </Grid>
                <Grid
                    container
                    item
                    direction="column"
                    justify="center"
                    alignItems="center"
                >
                    <Grid
                        container
                        item
                        direction="row"
                        justify="space-between"
                        alignItems="center"
                    >
                        <Grid
                            container
                            item
                            direction="column"
                            justify="center"
                            alignItems="center"
                        >
                            <Typography variant="h3" gutterBottom>
        	                    {worker.first_name} {worker.last_name}
        	                </Typography>
                            <Typography variant="h3" gutterBottom>
        	                    {worker.country}, {worker.city}
        	                </Typography>
                        </Grid>
                        <Grid
                            container
                            item
                            direction="column"
                            justify="center"
                            alignItems="center"
                        >
                            <div>
                                <Rating
                                    name="simple-controlled"
                                    value={worker.rating ? parseInt(worker.rating) : 0}
                                    onChange={(event, newValue) => {
                                        setRating(newValue);
                                    }}
                                />
                                <b>{worker.rating}</b>
                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}


const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(withRouter(Worker));