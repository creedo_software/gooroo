import '@babel/polyfill'

import * as React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import Homepage from './components/homepage'
import Login from './components/login'
import Register from './components/register'
import Dashboard from './components/dashboard'
import Workers from './components/workers'
import Worker from './components/worker'
import ProfileBasic from './components/profile/basic'
import ProfileWork from './components/profile/work'
import ProfilePolicies from './components/profile/policies'
import ProfileAvailability from './components/profile/availability'

import AboutUs from './components/static/about-us'
import ContactUs from './components/static/contact-us'
import FAQ from './components/static/faq'
import Help from './components/static/help'
import HowItWorks from './components/static/how-it-works'
import PrivacyPolicy from './components/static/privacy-policy'
import Terms from './components/static/terms'

import HeaderMenu from './components/common/header/menu'
import BottomMenu from './components/common/bottom/menu'

import { Authmanager } from './auth';

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import createRootReducer from './reducers'
import rootSaga from './sagas'
import { routerMiddleware, ConnectedRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import {
    HashRouter as Router,
    Switch,
    Route
} from "react-router-dom";

const history = createBrowserHistory()

const sagaMiddleware = createSagaMiddleware()
const store = createStore(createRootReducer(history), applyMiddleware(routerMiddleware(history), sagaMiddleware))
sagaMiddleware.run(rootSaga)

const theme = createMuiTheme(

{
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: '#282b2f',
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      //light: '#0066ff',
      main: '#3b858c',
      // dark: will be calculated from palette.secondary.main,
      //contrastText: '#ffcc00',
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    //contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    //tonalOffset: 0.2,
  },
}

);

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Router basename={'/'}>
                <ThemeProvider theme={theme}>
                    <HeaderMenu />
                    <Switch>
                        <Route exact path="/"><Homepage /></Route>
                        <Route exact path="/login"><Login /></Route>
                        <Route exact path="/sign-up"><Register /></Route>
                        <Route exact path="/dashboard"><Dashboard /></Route>
                        <Route exact path="/profile"><ProfileBasic /></Route>
                            <Route exact path="/profile/basic"><ProfileBasic /></Route>
                            <Route exact path="/profile/work"><ProfileWork /></Route>
                            <Route exact path="/profile/policies"><ProfilePolicies /></Route>
                            <Route exact path="/profile/availability"><ProfileAvailability /></Route>
                        <Route exact path="/workers"><Workers /></Route>
                        <Route exact path="/worker"><Worker /></Route>
                        <Route exact path="/about-us"><AboutUs /></Route>
                        <Route exact path="/contact-us"><ContactUs /></Route>
                        <Route exact path="/faq"><FAQ /></Route>
                        <Route exact path="/help"><Help /></Route>
                        <Route exact path="/how-it-works"><HowItWorks /></Route>
                        <Route exact path="/privacy-policy"><PrivacyPolicy /></Route>
                        <Route exact path="/terms"><Terms /></Route>
                    </Switch>
                    <BottomMenu />
                </ThemeProvider>
            </Router>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'),
)