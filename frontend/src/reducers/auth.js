import * as types from '../actions';

export const login = (state = {}, action) => {
    const response = action.response;

    switch(action.type) {
		case types.LOGIN_SUCCESS:
            return { ...state, response };
        case types.LOGIN_ERROR:
            return { ...state, ...{error: action.error}, response };
        default:
            return state;
    }
};

export const register = (state = {}, action) => {
    const response = action.response;

    switch(action.type) {
        case types.REGISTER_SUCCESS:
            return { ...state, response };
        case types.REGISTER_ERROR:
            return { ...state, ...{error: action.error}, response };
        default:
            return state;
    }
};

export const me = (state = {}, action) => {
    const response = action.response;

    switch(action.type) {
        case types.ME_SUCCESS:
            return { ...state, response };
        case types.ME_ERROR:
            return { ...state, ...{error: action.error}, response };
        default:
            return state;
    }
};