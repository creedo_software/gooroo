import * as types from '../actions';

export default function(state = [], action) {
    const response = action.response;

    switch(action.type) {
        case types.CATEGORIES_TREE_SUCCESS:
            return { ...state, response };
        case types.CATEGORIES_TREE_ERROR:
            return { ...state, ...{error: action.error}, response };
        default:
            return state;
    }
};