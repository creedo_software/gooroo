import * as types from '../actions';

export default function(state = [], action) {
    const response = action.response;

    switch(action.type) {
        case types.HOMEPAGE_INFO_SUCCESS:
            return { ...state, response };
        case types.HOMEPAGE_INFO_ERROR:
            return { ...state, ...{error: action.error}, response };
        default:
            return state;
    }
};