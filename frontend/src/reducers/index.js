import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import homepage from './homepage';
import categories from './categories';
import { login, register, me } from './auth';
import user from './user';
import worker from './worker';

const createRootReducer = (history) => combineReducers({
    homepage, categories, login, register, me, user, worker,
    
    router: connectRouter(history)
});

export default createRootReducer;