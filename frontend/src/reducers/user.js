import * as types from '../actions';

export default function(state = [], action) {
    const response = action.response;

    switch(action.type) {
        case types.UPDATE_PROFILE_SUCCESS:
            return { ...state, basic: response };
        case types.UPDATE_PROFILE_ERROR:
            return { ...state, ...{error: action.error}, basic: response };
        default:
            return state;
    }
};