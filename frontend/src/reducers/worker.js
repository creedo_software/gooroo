import * as types from '../actions';

export default function(state = [], action) {
    const response = action.response;

    switch(action.type) {
        case types.UPDATE_MY_WORKER_SUCCESS:
            return { ...state, my_update: response }
        case types.UPDATE_MY_WORKER_ERROR:
            return { ...state, ...{error: action.error}, my_update: response }
        case types.GET_MY_WORKER_SUCCESS:
            return { ...state, my_worker: response }
        case types.GET_MY_WORKER_ERROR:
            return { ...state, ...{error: action.error}, my_worker: response }
        case types.FIND_WORKERS_SUCCESS:
            return { ...state, find: response }
        case types.FIND_WORKERS_ERROR:
            return { ...state, ...{error: action.error}, find: response }
        case types.GET_WORKER_SUCCESS:
            return { ...state, worker: response }
        case types.GET_WORKER_ERROR:
            return { ...state, ...{error: action.error}, worker: response }
        default:
            return state;
    }
};