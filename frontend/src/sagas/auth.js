import { put, call } from 'redux-saga/effects';
import { login, register, me } from '../services/auth';

import * as types from '../actions'

export function* meSaga(payload) {
    try {
        const response = yield call(me, payload);
        yield put({ type: types.ME_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.ME_ERROR, error })
    }
}

export function* loginSaga(payload) {
    try {
        const response = yield call(login, payload);
        yield put({ type: types.LOGIN_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.LOGIN_ERROR, error })
    }
}

export function* registerSaga(payload) {
    try {
        const response = yield call(register, payload);
        yield put({ type: types.REGISTER_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.REGISTER_ERROR, error })
    }
}