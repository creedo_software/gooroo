import { put, call } from 'redux-saga/effects';
import { categoriesTree } from '../services/categories';

import * as types from '../actions'

export function* categoriesTreeSaga(payload) {
    try {
        const response = yield call(categoriesTree, payload);
        yield put({ type: types.CATEGORIES_TREE_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.CATEGORIES_TREE_ERROR, error })
    }
}