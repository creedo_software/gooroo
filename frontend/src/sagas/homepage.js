import { put, call } from 'redux-saga/effects';
import { homepageInfo } from '../services/homepage';

import * as types from '../actions'

export function* homepageInfoSaga(payload) {
    try {
        const response = yield call(homepageInfo, payload);
        yield put({ type: types.HOMEPAGE_INFO_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.HOMEPAGE_INFO_ERROR, error })
    }
}