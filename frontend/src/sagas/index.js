import { fork } from 'redux-saga/effects';
import {
    watchHomepageInfo, watchCategoriesTree, watchLogin, watchRegister,
    watchMe, watchUpdateProfile, watchUpdateMyWorker, watchGetMyWorkerSaga,
    watchFindWorkersSaga, watchGetWorkerSaga
} from './watchers';

export default function* startForman() {
    yield fork(watchHomepageInfo)
    yield fork(watchCategoriesTree)
    yield fork(watchLogin)
    yield fork(watchRegister)
    yield fork(watchMe)
    yield fork(watchUpdateProfile)
    yield fork(watchUpdateMyWorker)
    yield fork(watchGetMyWorkerSaga)
    yield fork(watchFindWorkersSaga)
    yield fork(watchGetWorkerSaga)
}