import { put, call } from 'redux-saga/effects';
import { updateProfile } from '../services/user';

import * as types from '../actions'

export function* updateProfileSaga(payload) {
    try {
        const response = yield call(updateProfile, payload);
        yield put({ type: types.UPDATE_PROFILE_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.UPDATE_PROFILE_ERROR, error })
    }
}