import { takeLatest } from 'redux-saga/effects';
import { homepageInfoSaga } from './homepage';
import { categoriesTreeSaga } from './categories';
import { loginSaga, registerSaga, meSaga } from './auth';
import { updateProfileSaga } from './user';
import { updateMyWorkerSaga, getMyWorkerSaga, getWorkerSaga, findWorkersSaga } from './worker';

import * as types from '../actions';

export function* watchHomepageInfo() {
    yield takeLatest(types.HOMEPAGE_INFO_REQUEST, homepageInfoSaga);
}

export function* watchCategoriesTree() {
    yield takeLatest(types.CATEGORIES_TREE_REQUEST, categoriesTreeSaga);
}

export function* watchLogin() {
    yield takeLatest(types.LOGIN_REQUEST, loginSaga);
}

export function* watchRegister() {
    yield takeLatest(types.REGISTER_REQUEST, registerSaga);
}

export function* watchMe() {
    yield takeLatest(types.ME_REQUEST, meSaga);
}

export function* watchUpdateProfile() {
    yield takeLatest(types.UPDATE_PROFILE_REQUEST, updateProfileSaga);
}

export function* watchUpdateMyWorker() {
    yield takeLatest(types.UPDATE_MY_WORKER_REQUEST, updateMyWorkerSaga);
}

export function* watchGetMyWorkerSaga() {
    yield takeLatest(types.GET_MY_WORKER_REQUEST, getMyWorkerSaga);
}

export function* watchGetWorkerSaga() {
    yield takeLatest(types.GET_WORKER_REQUEST, getWorkerSaga);
}

export function* watchFindWorkersSaga() {
    yield takeLatest(types.FIND_WORKERS_REQUEST, findWorkersSaga);
}


