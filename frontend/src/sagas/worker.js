import { put, call } from 'redux-saga/effects';
import { updateMyWorker, getMyWorker, findWorkers, getWorker } from '../services/worker';

import * as types from '../actions'

export function* updateMyWorkerSaga(payload) {
    try {
        const response = yield call(updateMyWorker, payload);
        yield put({ type: types.UPDATE_MY_WORKER_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.UPDATE_MY_WORKER_ERROR, error })
    }
}

export function* getMyWorkerSaga(payload) {
    try {
        const response = yield call(getMyWorker, payload);
        yield put({ type: types.GET_MY_WORKER_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.GET_MY_WORKER_ERROR, error })
    }
}

export function* findWorkersSaga(payload) {
    try {
        const response = yield call(findWorkers, payload);
        yield put({ type: types.FIND_WORKERS_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.FIND_WORKERS_ERROR, error })
    }
}

export function* getWorkerSaga(payload) {
    try {
        const response = yield call(getWorker, payload);
        yield put({ type: types.GET_WORKER_SUCCESS, response })
    } catch(error) {
        yield put({ type: types.GET_WORKER_ERROR, error })
    }
}