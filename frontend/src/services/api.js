import Authmanager from 'react-authmanager';

export const fetchAPI = (url, params) => {
    const API_ENDPOINT = process.env.API_HOST + '/api/' + url;

    let parameters = params
    if(!parameters['headers']){
        parameters['headers'] = {}
    }
	parameters['headers']['Content-Type'] = 'application/json'
    
    return fetch(API_ENDPOINT, parameters)
        .then(response => {
            return response.json();
        })
        .then(json => {
            return json;
        });
};

export const fetchAPISecure = (url, params) => {
    const parameters = Object.assign(
        {}, 
        params,
        {
            headers: {
                Authorization: 'Bearer ' + Authmanager.getToken() 
            }
        }
    )
     
    return fetchAPI(url, parameters)
};