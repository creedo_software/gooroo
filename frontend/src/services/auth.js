import { fetchAPI, fetchAPISecure } from './api'

export const login = (request) => fetchAPI('login', {
	method: 'POST',
    body: JSON.stringify(request.params)
})

export const register = (request) => fetchAPI('register', {
	method: 'POST',
    body: JSON.stringify(request.params)
})

export const me = (request) => fetchAPISecure('me', {
	method: 'GET',
})


