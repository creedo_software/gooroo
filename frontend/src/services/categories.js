import { fetchAPI } from './api'

export const categoriesTree = (request) => fetchAPI('category/tree', {
	method: 'GET'
})