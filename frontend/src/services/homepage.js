import { fetchAPI } from './api'

export const homepageInfo = (request) => fetchAPI('info/homepage', {
	method: 'GET'
})