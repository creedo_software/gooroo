import { fetchAPI, fetchAPISecure } from './api'

export const updateProfile = (request) => fetchAPISecure('user', {
	method: 'POST',
    body: JSON.stringify(request.params)
})

