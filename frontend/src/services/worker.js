import { fetchAPI, fetchAPISecure } from './api'

export const updateMyWorker = (request) => fetchAPISecure('me/worker', {
	method: 'POST',
    body: JSON.stringify(request.params)
})

export const getMyWorker = (request) => fetchAPISecure('me/worker', {
	method: 'GET',
})

export const getWorker = (request) => fetchAPISecure('worker/' + request.params.id, {
	method: 'GET',
})

export const findWorkers = (request) => fetchAPISecure('workers', {
	method: 'GET',
})




