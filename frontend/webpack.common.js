const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack');

require('dotenv').config()

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, 'src/main.js'),
  output: {
        filename: 'main.js',
        publicPath: '/',
    },
  plugins: [
      new webpack.DefinePlugin({  // plugin to define global constants
          PUBLIC_URL: JSON.stringify(process.env.PUBLIC_URL),
          API_HOST: JSON.stringify(process.env.API_HOST)
      }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({ template: path.resolve(__dirname, 'public/index.html') }),
    new Dotenv(),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
      },
      {
         test: /\.(png|svg|jpg|gif)$/,
         use: [
           'file-loader',
         ],
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
              modules: {
                localIdentName: "[local]___[hash:base64:5]"
              }
            }
          },
          {
            loader: "less-loader"
          }
        ]
      }
    ],
  },
  devServer: {
    port: 5000,
    hot: true,
    historyApiFallback: true,
  },
}
